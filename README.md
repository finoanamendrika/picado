![Picado](./src/assets/img/picado-favicon.png?raw=true)

# Picado

Picado is an image processing software made with the framework [ElectronJS](https://www.electronjs.org/). 

## Installation

**Note:** Picado requires `NodeJS:~14` or above.

Use the `npm install` command to install the dependencies.

```bash
npm install
```

## Launch

To launch the Picado project, you need to run the following commands:

-In development mode

```bash
npm start
```

-In production (generate an executable binary file)

```bash
npm run make
```

NB: the build will be placed in the folder *out / picado-win32-x64* of the project root under the name of *picado.exe*.

## Contribution

Picado is an OpenSource project, any contribution would be welcomed with joy. For major changes, please contact the authors of the project.

## Team

[Finoana Mendrika](https://gitlab.com/finoanamendrika) | [Rabefialy Jonathan](https://gitlab.com/Jogasy)

## License
[MIT](https://choosealicense.com/licenses/mit/)