// tortue de Papert Contour d'image

const { Matrix } = require('ml-matrix');

var matriceClone;
var tabstockage = [];

function fusion(gauche, droite){
    var tab = [], l = 0, r = 0;
    while (l < gauche.length && r < droite.length){
        if (gauche[l] < droite[r]){
            tab.push(gauche[l++]);
        } else {
            tab.push(droite[r++]);
        }
    }
    return tab.concat(gauche.slice(l)).concat(droite.slice(r));
  }
  
  function tri(tab){
    if (tab.length < 2) {
        return tab;
    }
    var mid = Math.floor(tab.length / 2),
        droite = tab.slice(mid),
        gauche = tab.slice(0, mid),
        p = fusion(tri(gauche), tri(droite));
    p.unshift(0, tab.length);
    tab.splice.apply(tab, p);
    return tab;
  }

function Avancer(x,y, matrice = new Matrix([]), arret_x, arret_y) {
    let p = matrice.get(x, y + 1);
    if( x == arret_x && y + 1 == arret_y) {
        //console.log(arret_x, arret_y);
        tabstockage.push(arret_x + (arret_y / 10000));
        matriceClone.set(arret_x , arret_y, 1);
        //console.log("Terminer!");
        //return matrice2DClone;
    }else {
        if(p == 1){
            matriceClone.set(x, y + 1, 1);
            //console.log(x, y + 1);
            tabstockage.push(x + ((y + 1) / 10000));
            AvancerGauche(x, y + 1, matrice, arret_x, arret_y);
        }else{
            AvancerDroite(x, y + 1, matrice, arret_x, arret_y);
        }
    }
}

function AvancerGauche(x,y, matrice = new Matrix([]), arret_x, arret_y) {
    let p = matrice.get(x - 1, y);
    if( x - 1 == arret_x && y == arret_y) {
        //console.log(arret_x, arret_y);
        tabstockage.push(arret_x + (arret_y / 10000));
        matriceClone.set(arret_x , arret_y, 1);
        //console.log("Terminer!");
        //return matrice2DClone;
    }else {
        if(p == 1){
            matriceClone.set(x - 1, y, 1);
            //console.log(x - 1, y);
            tabstockage.push(x - 1 + (y / 10000));
            Reculer(x - 1, y, matrice, arret_x, arret_y);
        }else{
            Avancer(x - 1, y, matrice, arret_x, arret_y);
        }
    }
}

function AvancerDroite(x,y, matrice = new Matrix([]), arret_x, arret_y) {
    let p = matrice.get(x + 1, y);
    if( x + 1 == arret_x && y == arret_y) {
        //console.log(arret_x, arret_y);
        tabstockage.push(arret_x + (arret_y / 10000));
        matriceClone.set(arret_x , arret_y, 1);
        //console.log("Terminer!");
        //return matrice2DClone;
    }else {
        if (p == 1){
            matriceClone.set(x + 1, y, 1);
            //console.log(x + 1, y);
            tabstockage.push(x + 1 + (y / 10000));
            Avancer(x + 1, y, matrice, arret_x, arret_y);
        }else{
            Reculer(x + 1, y, matrice, arret_x, arret_y);
        }
    }
}

function Reculer(x,y, matrice = new Matrix([]), arret_x, arret_y) {
    let p = matrice.get(x, y - 1);
    if( x == arret_x && y - 1 == arret_y) {
        //console.log(arret_x, arret_y);
        tabstockage.push(arret_x + (arret_y / 10000));
        matriceClone.set(arret_x , arret_y, 1);
        //console.log("Terminer!");
        //return matrice2DClone;
    }else {
        if(p == 0) {
            ReculerDroite(x , y - 1, matrice, arret_x, arret_y);
        }else{
            matriceClone.set(x, y - 1, 1);
            //console.log(x, y - 1);
            tabstockage.push(x + ( (y - 1) / 10000));
            ReculerGauche(x , y - 1, matrice, arret_x, arret_y);
        }
    }
}

function ReculerDroite(x,y, matrice = new Matrix([]), arret_x, arret_y) {
    let p = matrice.get(x - 1, y);
    if( x - 1 == arret_x && y == arret_y) {
        //console.log(arret_x, arret_y);
        tabstockage.push(arret_x + (arret_y / 10000));
        matriceClone.set(arret_x , arret_y, 1);
        //console.log("Terminer!");
        //return matrice2DClone;
    }else {
        if(p == 1) {
            matriceClone.set(x - 1, y, 1);
            //console.log(x - 1, y);
            tabstockage.push(x - 1 + (y / 10000));
            Reculer(x - 1, y, matrice, arret_x, arret_y);
        }else{
            Avancer(x - 1, y, matrice, arret_x, arret_y);
        }
    }
}

function ReculerGauche(x,y, matrice = new Matrix([]), arret_x, arret_y) {
    let p = matrice.get(x + 1, y);
    if( x + 1 == arret_x && y == arret_y) {
        //console.log(arret_x, arret_y);
        tabstockage.push(arret_x + (arret_y / 10000));
        matriceClone.set(arret_x , arret_y, 1);
        //console.log("Terminer!");
        //return matrice2DClone;
    }else {
        if(p == 0) {
            Reculer(x + 1, y, matrice, arret_x, arret_y);
        }else{
            matriceClone.set(x + 1, y, 1);
            //console.log(x + 1, y);
            tabstockage.push(x + 1 + (y / 10000));
            Avancer(x + 1, y, matrice, arret_x, arret_y);
        }
    }
}

function Contour(matrice = new Matrix([])) {
    // //ajout des lignes 
    // matrice.addRow(0, Matrix.zeros(1, matrice.columns));
    // matrice.addRow(matrice.rows, Matrix.zeros(1, matrice.columns));
    //ajout des colonnes
    // matrice.addColumn(0, Matrix.zeros(matrice.rows, 1));
    // matrice.addColumn(matrice.columns, Matrix.zeros(matrice.rows, 1));
    //console.log(matrice);
    for(let i = 0; i < matrice.columns; i++) {
        matrice.set(0, i, 0);
    } 
    for (let index = 0; index < matrice.columns; index++) {
        matrice.set(matrice.rows - 1, index, 0);
    }
    for(let i = 0; i < matrice.rows; i++) {
        matrice.set(i, 0, 0);
    } 
    for(let i = 0; i < matrice.rows; i++) {
        matrice.set(i, matrice.rows - 1, 0);
    }
    matriceClone = Matrix.zeros(matrice.rows , matrice.columns);
    for (let ligne = 0; ligne < matrice.rows ; ligne++){
        if (ligne == matrice.rows - 1) {
            return matriceClone;
        }
        for (let col = 0; col < matrice.columns ; col++ ) {
            var tabTrier = tri(tabstockage);
            var fin = tabTrier.length - 1;
            if(tabTrier.length == 0) {
                col = col;
            }else{
                var col2 = rechercheTab(ligne, col, tabTrier, tabTrier, 0, fin, matrice);
                col = col2;
            }
            if( matrice.get(ligne, col) == 1){
                matriceClone.set(ligne, col, 1);
                tabstockage.push(ligne + (col / 10000));
                AvancerGauche(ligne, col, matrice, ligne, col);
            }
        }
    }
}

function rechercheTab(ligne, col, tabOriginal = [], tab = [], debut, fin, matrice = new Matrix([])) {
    // for ( let x = 0 ; x < tab.length; x++ ) {
    //     if ( tab[x] == ligne + (col / 10000) ) {
    //         return rechercheTab(ligne, col + 1, tab, matrice);
    //     } 
    // }
    // //s'il n'est pas encore fans le tableau , on verifie si son voisin a guache et a droite est dans le tableau
    // if ( matrice.get(ligne, col-1) == 1 && matrice.get(ligne, col+1) == 1 ) {
    //     return rechercheTab(ligne, col + 2, tab, matrice);
    // }
    // return col;
    const start = debut;
    const end = fin;
    var valeur = 0;
    var milieu = 0;
    
    // recherche par dichotomie
    valeur = ligne + (col / 10000);
    milieu = parseInt((tab.length / 2));
    if(milieu == 1) {
        if (tab[0] == valeur || tab[1] == valeur) {
            return rechercheTab(ligne, col + 1, tabOriginal, tabOriginal, 0, tabOriginal.length - 1, matrice);
        }else if ( matrice.get(ligne, col) == 1 && matrice.get(ligne, col-1) == 1 && matrice.get(ligne, col+1) == 1 ) {
            return rechercheTab(ligne, col + 2, tabOriginal, tabOriginal, 0, tabOriginal.length - 1, matrice);
        }
    }else if(milieu == 0){
        if (tab[0] == valeur) {
            return rechercheTab(ligne, col + 1, tabOriginal, tabOriginal, 0, tabOriginal.length - 1 - 1, matrice);
        }else if ( matrice.get(ligne, col-1) == 1 && matrice.get(ligne, col+1) == 1 ) {
            return rechercheTab(ligne, col + 2, tabOriginal, tabOriginal, 0, tabOriginal.length - 1 - 1, matrice);
        }
    }else if(tab[milieu] == valeur) {
        return rechercheTab(ligne, col + 1, tabOriginal, tabOriginal, 0, tabOriginal.length - 1, matrice);
    }else if(tab[milieu] < valeur) {
        return rechercheTab(ligne, col, tabOriginal, tab.slice(milieu + 1, end + 1), 0, tab.length - 1, matrice);
    }else {
        return rechercheTab(ligne, col, tabOriginal, tab.slice(start , milieu), 0, tab.length - 1, matrice);
    }

    return col;
}

// supposons la matrice apres binarisation
var matrix = new Matrix([
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,1,1,1,0,1,1,1,0,1,0,1,1,0,0,0,1,1,1,0,1,1,1,0,1,0,1,1,0],
    [0,1,1,1,1,0,1,1,1,0,0,1,1,1,0,0,1,1,1,1,0,1,1,1,0,0,1,1,1,0],
    [0,1,1,1,1,0,1,1,1,0,0,1,1,1,0,0,1,1,1,1,0,1,1,1,0,0,1,1,1,0],
    [0,0,1,1,0,1,0,1,1,0,0,0,1,1,0,0,0,1,1,0,1,0,1,1,0,0,0,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,1,1,0,0,1,1,1,0,0,0,1,1,0,0,0,1,1,0,0,1,1,1,0,0,0,1,1,0],
    [0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0],
    [0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0],
    [0,0,1,1,0,0,0,1,1,0,0,0,1,1,0,0,0,1,1,0,0,0,1,1,0,0,0,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,1,1,0,0,1,1,1,0,0,0,1,1,0,0,0,1,1,0,0,1,1,1,0,0,0,1,1,0],
    [0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0],
    [0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0,0,1,1,1,0],
    [0,0,1,1,0,0,0,1,1,0,0,0,1,1,0,0,0,1,1,0,0,0,1,1,0,0,0,1,1,0],
    [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
    [0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0]
]);

console.log(Contour(matrix));
//console.log(tabstockage);