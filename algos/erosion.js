const { Matrix } = require('ml-matrix');

function erosion(x, y, matrice = new Matrix([]), masque = new Matrix([])) {
    clone = Matrix.zeros(masque.rows, masque.columns);
    const q = parseInt(Math.sqrt(masque.size) / 2);
    for(var line = - parseInt(masque.columns / 2); line < parseInt(masque.columns / 2)+1; line++ ) {
        // console.log("line", line);
        for (let col = - parseInt(masque.columns / 2); col < parseInt(masque.columns / 2)+1; col++) {
            // console.log("col", col);
            lx = x - line;
            cy = y - col;
            if(lx < 0 || cy < 0 || lx == matrice.rows || cy == matrice.columns) {
                clone.set( - line + q , - col + q, 0);
            }else{
                clone.set( - line + q, - col + q, matrice.get(lx, cy));
            }
        }
    }
    for(let ligne = 0; ligne < masque.rows; ligne++) {
        for(let col = 0; col < masque.columns; col++) {
            if (masque.get(ligne, col) != clone.get(ligne, col)) {
                return 0;
            }
        }
    }
    return 1;
}

function erosionImage( matrice = new Matrix([]), masque = new Matrix([])) {
    let mx = matrix.clone();
    for(let y = 0; y < matrix.columns; y++){
        for(let x = 0; x < matrix.rows; x++){
            mx.set(x, y, erosion(x,y,matrix,masque));
        }
    }
    //console.log(mx);
    return mx;
}
    

var matrix = new Matrix([
    [1,1,1,1,0,0],
    [1,1,1,1,1,1],
    [1,1,1,1,1,1],
    [1,1,1,1,0,0]    
]);

var masque = new Matrix([
    [1, 1, 1],
    [1, 1, 1],
    [1, 1, 1]
]);
console.log(erosionImage(matrix, masque));