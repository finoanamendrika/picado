var canvas = document.getElementById("canvas");
var ctx = canvas.getContext('2d');
var originalData;
var imageDataNow;
var imageDataBefore;
var historique = [];

function pushHistorique(imageData) {
    historique.push(imageData);
    console.log(historique);
}

document.getElementById("btnLoadImage").onclick = () => {
    var input = document.createElement('input');
    input.type = 'file';
    input.accept = '.png,.jpg,.jpeg';
    input.click();

    input.onchange = (event) => {
        var path = URL.createObjectURL(event.target.files[0]);
        var img = document.createElement('img');

        img.onload = function() {
            var imgHeight = parseInt(window.innerHeight - 100);
            var imgWidth = parseInt(((window.innerHeight - 100) * img.width) / img.height);;

            var canvas = document.getElementById('canvas');
            canvas.height = imgHeight;
            canvas.width = imgWidth;
            ctx.drawImage(img, 0, 0, canvas.width, canvas.height);
            originalData = ctx.getImageData(0, 0, canvas.width, canvas.height);
            pushHistorique(originalData);
        }
        img.src = path;
    }
}

function original() {
    ctx.putImageData(originalData, 0, 0);
}

// function getActionNow() {
//     let { width, height } = canvas.getBoundingClientRect();
//     imageDataNow = ctx.getImageData(0, 0, width, height);
// }

// function getActionBefore() {
//     let { width, height } = canvas.getBoundingClientRect();
//     imageDataBefore = ctx.getImageData(0, 0, width, height);
// }

var invert = function() {
    // getActionBefore();
    let { width, height } = canvas.getBoundingClientRect();
    const imageData = ctx.getImageData(0, 0, width, height);
    const data = imageData.data;
    for (var i = 0; i < data.length; i += 4) {
        data[i] = 255 - data[i];
        data[i + 1] = 255 - data[i + 1];
        data[i + 2] = 255 - data[i + 2];
    }
    ctx.putImageData(imageData, 0, 0);
    pushHistorique(imageData);
    // getActionNow();
}

function back() {
    ctx.putImageData(historique[historique.length - 2], 0, 0);
}

function suivant() {
    ctx.putImageData(historique[historique.length - 1], 0, 0);
}

function calculHistogrammeImage() {
    let { width, height } = canvas.getBoundingClientRect();
    var imgData = ctx.getImageData(0, 0, width, height);
    var data = imgData.data;
    var classification = [];
    for (let i = 0; i < data.length; i++) {
        let index = classification.findIndex(function(e) { return e == data[i]; });
        if (index == -1) {
            classification.push(data[i]);
        }
    }

    classification = classification.sort(function(a, b) { return a - b });
    var histogrammeImage = new Array(classification.length);
    for (let i = 0; i < histogrammeImage.length; i++) {
        histogrammeImage[i] = 0;
    }
    for (let i = 0; i < data.length; i++) {
        let index = classification.findIndex(function(e) { return e == data[i] });
        histogrammeImage[index]++;
    }
    return histogrammeImage;
}

function histoCumul(histogrammeImage) {
    var sommeHistogrammeImage = 0;
    var histogrammeCumule = [];
    for (let i = 0; i < histogrammeImage.length; i++) {
        sommeHistogrammeImage += histogrammeImage[i];
        histogrammeCumule.push(sommeHistogrammeImage);
    }
    return histogrammeCumule
}

function calculSeuil2() {
    var histogrammeImage = calculHistogrammeImage();
    var cumul = histoCumul(histogrammeImage);
    var result = [];
    var seuil = 0;
    for (let index = 0; index < histogrammeImage.length; index++) {
        var weightBack = 0;
        var meanBack = 0;
        var sommeForMeanBack = 0;
        var varianceBack = 0;
        var sommeForVarianceBack = 0;
        var sommeForWeighBack = 0;
        var weightForm = 0
        var meanForm = 0;
        var sommeForMeanForm = 0;
        var varianceForm = 0;
        var sommeForVarianceForm = 0;
        var sommeForWeightForm = 0;
        for (let i = 0; i < index; i++) {
            sommeForMeanBack += histogrammeImage[i] * i;
            sommeForWeighBack += histogrammeImage[i];
        }
        meanBack = sommeForMeanBack / cumul[index];
        for (let i = 0; i < index; i++) {
            sommeForVarianceBack += Math.pow((i - meanBack), 2) * histogrammeImage[i];
        }
        varianceBack = sommeForVarianceBack / cumul[index];

        for (let i = index; i < histogrammeImage.length; i++) {
            sommeForMeanForm += histogrammeImage[i] * i;
            sommeForWeightForm += histogrammeImage[i];
        }
        meanForm = sommeForMeanForm / sommeForWeightForm;

        for (let i = index; i < histogrammeImage.length; i++) {
            sommeForVarianceForm += Math.pow((i - meanForm), 2) * histogrammeImage[i];
        }

        varianceForm = sommeForVarianceForm / sommeForWeightForm;
        weightBack = sommeForWeighBack / cumul[cumul.length - 1];
        weightForm = sommeForWeightForm / cumul[cumul.length - 1];
        result.push(weightBack * varianceBack + weightForm * varianceForm);
    }
    /*console.log(result.indexOf(Math.min(...result)));
    console.log(Math.min(...result));*/
    seuil = result.indexOf(Math.min(...result));
    console.log("seuil", seuil);
    return seuil;
}

var amelioration = function() {
    let { width, height } = canvas.getBoundingClientRect();
    const imageData = ctx.getImageData(0, 0, width, height);
    const data = imageData.data;
    var histogrammeImage = calculHistogrammeImage();
    var histogrammeCumule = calculHistoCumul();
    var egalisationHistogramme = Math.round(histogrammeCumule[histogrammeCumule.length - 1] / histogrammeCumule.length);
    var couleurImageAuto = [];
    for (let i = 0; i < data.length; i++) {
        // couleurImageAuto.push((data[data.length - 1] - 1) * histogrammeCumule[0]);
        data[i] = (data[data.length - 1] - 1) * histogrammeCumule[i];
        data[i + 1] = (data[data.length - 1] - 1) * histogrammeCumule[i];
        data[i + 2] = (data[data.length - 1] - 1) * histogrammeCumule[i];
    }
    ctx.putImageData(imageData, 0, 0);
}

// function range() {
//     // value = 0; min = -50; max = 50
//     //console.log(Math.max.apply(null, histogrammeImage)); MAX TABLEAU
// }

var grey = function() {
    let { width, height } = canvas.getBoundingClientRect();
    const imageData = ctx.getImageData(0, 0, width, height);
    const data = imageData.data;
    for (var i = 0; i < data.length; i += 4) {
        let grey = (data[i] + data[i + 1] + data[i + 2]) / 3;
        data[i] = grey;
        data[i + 1] = grey;
        data[i + 2] = grey;
    }
    ctx.putImageData(imageData, 0, 0);
    pushHistorique(imageData);
}

function calculSeuil() {
    var histogrammeImage = calculHistogrammeImage();
    var otsu = 0;
    var omega1, mu1, omega2, mu2;
    var sigma, maxsigma = 0;
    for (let t = 0; t < histogrammeImage.length; t++) {
        // En dessous du seuil courant 
        omega1 = 0;
        mu1 = 0;
        for (let i = 0; i <= t; i++) {
            omega1 += histogrammeImage[i];
            mu1 += histogrammeImage[i] * i;
        }
        mu1 /= omega1;

        // Au dessus du seuil courant
        omega2 = 0;
        mu2 = 0;
        for (let i = t + 1; i < histogrammeImage.length; i++) {
            omega2 += histogrammeImage[i];
            mu2 += histogrammeImage[i] * i;

        }
        mu2 /= omega2;

        /* Cacul de la variance inter-classe */
        sigma = omega1 * omega2 * Math.pow(mu1 - mu2, 2);
        if (sigma > maxsigma) {
            otsu = t;
            maxsigma = sigma;
        }
    }
    console.log(otsu);
    return otsu;
}

var binaire = function() {
    var seuil = calculSeuil2();
    grey();
    let { width, height } = canvas.getBoundingClientRect();
    const imageData = ctx.getImageData(0, 0, width, height);
    const data = imageData.data;
    for (var i = 0; i < data.length; i += 4) {
        if (data[i] < seuil) {
            data[i] = 0;
        } else {
            data[i] = 255;
        }
        if (data[i + 1] < seuil) {
            data[i + 1] = 0;
        } else {
            data[i + 1] = 255;
        }
        if (data[i + 2] < seuil) {
            data[i + 2] = 0;
        } else {
            data[i + 2] = 255;
        }
    }
    ctx.putImageData(imageData, 0, 0);
}

function pipette(event) {
    var color = document.getElementById('couleur');
    var textColor = document.getElementById('text-color');
    var x = event.layerX;
    var y = event.layerY;
    var pixel = ctx.getImageData(x, y, 1, 1);
    var data = pixel.data;
    var rgba = 'rgba(' + data[0] + ', ' + data[1] + ', ' + data[2] + ', ' + (data[3] / 255) + ')';
    color.style.background = rgba;
    textColor.textContent = rgba;
}
canvas.addEventListener('mousemove', pipette);

function zoom() {
    var zoom = document.getElementById('zoom');
    var zoomctx = zoom.getContext('2d');
    var zoomBtn = document.getElementById('zoomBtn');
    var toggleSmoothing = function() {
        zoomctx.imageSmoothingEnabled = this.checked;
        zoomctx.mozImageSmoothingEnabled = this.checked;
        zoomctx.webkitImageSmoothingEnabled = this.checked;
        zoomctx.msImageSmoothingEnabled = this.checked;
    };
    zoomBtn.addEventListener('change', toggleSmoothing);

    var zoom = function(event) {
        var x = event.layerX;
        var y = event.layerY;
        zoomctx.drawImage(canvas, Math.abs(x - 5), Math.abs(y - 5), 10, 10, 0, 0, 200, 200);
    };
    canvas.addEventListener('mousemove', zoom);
}

function verifRangeLuminosite() {
    original();
    var luminosite = parseInt(document.getElementById('luminositeRange').value);
    let { width, height } = canvas.getBoundingClientRect();
    const imageData = ctx.getImageData(0, 0, width, height);
    const data = imageData.data;
    for (let i = 0; i < data.length; i += 4) {
        data[i] += luminosite;
        data[i + 1] += luminosite;
        data[i + 2] += luminosite;
    }
    ctx.putImageData(imageData, 0, 0);
}

function verifRangeContraste() {
    original();
    var contrasteRange = document.getElementById('contrasteRange');
    var contrast = (contrasteRange.value / 100) + 1;
    var intercept = 128 * (1 - contrast);
    let { width, height } = canvas.getBoundingClientRect();
    const imageData = ctx.getImageData(0, 0, width, height);
    const data = imageData.data;
    for (let i = 0; i < data.length; i += 4) {
        data[i] = data[i] * contrast + intercept;
        data[i + 1] = data[i + 1] * contrast + intercept;
        data[i + 2] = data[i + 2] * contrast + intercept;
    }
    ctx.putImageData(imageData, 0, 0);
}

function contraste() {
    verifRangeLuminosite();
    var contrasteRange = document.getElementById('contrasteRange');
    var contrast = (contrasteRange.value / 100) + 1;
    var intercept = 128 * (1 - contrast);
    let { width, height } = canvas.getBoundingClientRect();
    const imageData = ctx.getImageData(0, 0, width, height);
    const data = imageData.data;
    for (let i = 0; i < data.length; i += 4) {
        data[i] = data[i] * contrast + intercept;
        data[i + 1] = data[i + 1] * contrast + intercept;
        data[i + 2] = data[i + 2] * contrast + intercept;
    }
    ctx.putImageData(imageData, 0, 0);
}

function luminosite() {
    verifRangeContraste();
    var luminosite = parseInt(document.getElementById('luminositeRange').value);
    let { width, height } = canvas.getBoundingClientRect();
    const imageData = ctx.getImageData(0, 0, width, height);
    const data = imageData.data;
    for (let i = 0; i < data.length; i += 4) {
        data[i] += luminosite;
        data[i + 1] += luminosite;
        data[i + 2] += luminosite;
    }
    ctx.putImageData(imageData, 0, 0);
}

var transparence = function() {
    var transparenceRange = document.getElementById('transparenceRange').value;
    if (transparenceRange == 0) {
        original();
    } else {
        let { width, height } = canvas.getBoundingClientRect();
        const imageData = ctx.getImageData(0, 0, width, height);
        const data = imageData.data;
        for (let i = 0; i < data.length; i += 4) {
            data[i] = data[i] * transparenceRange.value;
            data[i + 1] = data[i + 1] * transparenceRange.value;
            data[i + 2] = data[i + 2] * transparenceRange.value;
        }
        ctx.putImageData(imageData, 0, 0);
    }
}

function etalementDynamique() {
    var max = 200;
    var min = 12;
    let { width, height } = canvas.getBoundingClientRect();
    const imageData = ctx.getImageData(0, 0, width, height);
    const data = imageData.data;

    for (let i = 0; i < data.length; i += 4) {
        data[i] = (255 * (data[i] - min)) / (max - min);
        data[i + 1] = (255 * (data[i + 1] - min)) / (max - min);
        data[i + 2] = (255 * (data[i + 2] - min)) / (max - min);
    }

    ctx.putImageData(imageData, 0, 0);
}

// function getImageArray(data = [], width, nrbPxl) {
//     var result = [];
//     for (let i = 0; i < data.length; i += width * nrbPxl) {
//         result.push(data.slice(i, i + (width * nrbPxl)));
//     }
//     return new Matrix(result);
// }

function saturation() {
    original();
    var saturationValue = saturationRange.value;
    let { width, height } = canvas.getBoundingClientRect();
    const imageData = ctx.getImageData(0, 0, width, height);
    const data = imageData.data;

    var sv = saturationValue / 10;

    var luminanceRouge = 0.3086;
    var luminanceGreen = 0.6094;
    var luminanceBleu = 0.0820;

    var az = (1 - sv) * luminanceRouge + sv;
    var bz = (1 - sv) * luminanceGreen;
    var cz = (1 - sv) * luminanceBleu;
    var dz = (1 - sv) * luminanceRouge;
    var ez = (1 - sv) * luminanceGreen + sv;
    var fz = (1 - sv) * luminanceBleu;
    var gz = (1 - sv) * luminanceRouge;
    var hz = (1 - sv) * luminanceGreen;
    var iz = (1 - sv) * luminanceBleu + sv;

    for (let i = 0; i < data.length; i += 4) {
        var saturationRed = (az * data[i] + bz * data[i + 1] + cz * data[i + 2]);
        var saturationGreen = (dz * data[i] + ez * data[i + 2] + fz * data[i + 2]);
        var saturationBlue = (gz * data[i] + hz * data[i + 2] + iz * data[i + 2]);

        data[i] = saturationRed;
        data[i + 1] = saturationGreen;
        data[i + 2] = saturationBlue;
    }
    ctx.putImageData(imageData, 0, 0);

}