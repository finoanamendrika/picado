function rotate(data = [], width, height) {
    var posSrc, posDest;
    for (var y = 0, h = height; y < h; y++) {
        for (var x = 0, w = width; x < w; x++) {
            posSrc = conversion(x, y, width);
            posDest = conversion(x, h - y, width);
            for (var i = 0; i < 4; i++) {
                data[posDest + i] = data[posSrc + i];
            }
        }
    }
    return data;
}