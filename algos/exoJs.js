//const { createServer } = require('http');
const { Matrix } = require("ml-matrix");

var matrice = new Matrix([
  [12,32,25,14,12,32,25,14,1,2,3,4,25,14,1,2],
  [11,22,33,44,12,32,25,14,4,5,6,7,25,14,1,2],
  [25,47,89,55,12,32,25,14,7,8,9,10,25,14,1,2],
  [25,47,58,69,12,32,25,14,2,5,8,9,25,14,1,2],
  [25,36,54,89,12,32,25,14,14,7,8,9,25,14,1,2]
]);

var masque = new Matrix([[1, 0, 2],
  [2, 1, 0],
  [1, 0, 3]]);

var rouge = Matrix.zeros(matrice.rows, (matrice.columns) /4);
var vert = Matrix.zeros(matrice.rows, (matrice.columns) /4);
var bleu = Matrix.zeros(matrice.rows, (matrice.columns) /4);
var opacite = Matrix.zeros(matrice.rows, (matrice.columns) /4);

for (let ligne = 0; ligne < matrice.rows; ligne++) {
  for (let col = 0; col < matrice.columns; col+=4){
      rouge.set(ligne, parseInt(col/4), matrice.get(ligne, col));
  }
}

var rougeFiltre = Filtrage_lineaire_image(rouge, masque);

for (let ligne = 0; ligne < matrice.rows; ligne++) {
  for (let col = 1; col < matrice.columns; col+=4){
      vert.set(ligne, parseInt(col/4), matrice.get(ligne, col));
  }
}

var vertFiltre = Filtrage_lineaire_image(vert, masque);

for (let ligne = 0; ligne < matrice.rows; ligne++) {
  for (let col = 2; col < matrice.columns; col+=4){
      bleu.set(ligne, parseInt(col/4), matrice.get(ligne, col));
  }
}

var bleuFiltre = Filtrage_lineaire_image(bleu, masque);

for (let ligne = 0; ligne < matrice.rows; ligne++) {
  for (let col = 3; col < matrice.columns; col+=4){
      opacite.set(ligne, parseInt(col/4), matrice.get(ligne, col));
  }
}


for (let ligne = 0; ligne < matrice.rows; ligne++) {
  for (let col = 0; col < matrice.columns; col+=4) {
    matrice.set(ligne, col, rougeFiltre.get(ligne, parseInt(col / 4)));
    matrice.set(ligne, col + 1, vertFiltre.get(ligne, parseInt(col / 4)));
    matrice.set(ligne, col + 2, bleuFiltre.get(ligne, parseInt(col / 4)));
    matrice.set(ligne, col + 3, opacite.get(ligne, parseInt(col / 4)));
  }
}

console.log("rouge", rouge);
console.log("vert", vert);
console.log("bleu", bleu);
console.log("opacite", opacite);

//console.log("matrice", matrice);

console.log("rougeFiltre", rougeFiltre);
console.log("vertFiltre", vertFiltre);
console.log("bleuFiltre", bleuFiltre);

console.log("matrice", matrice);

function Filtrage_lineaire_image(matrix = new Matrix([]), masque = new Matrix([])){
  let mx = matrix.clone();
  for(let y = 0; y < matrix.columns; y++){
      for(let x = 0; x < matrix.rows; x++){
          mx.set(x,y,Filtrage_lineaire_point(x,y,matrix,masque));
      }
  }
  //console.log(mx);
  return mx;
}

function Filtrage_lineaire_point(x,y,s = new Matrix([]),M = new Matrix([])) {
  const q = parseInt(Math.sqrt(M.size) / 2);
  let somme = 0.0;
  for(let col = - parseInt(M.columns / 2) ; col < (parseInt(M.columns / 2)+1); col++){
    for(let l = - parseInt(M.columns / 2) ; l < (parseInt(M.columns / 2)+1) ; l++){
    lx = x - l; // index de lignecxcxxxxxxxxxxxxxxxxxx     
    cy = y - col; // index de la colonne
    if(lx < 0 || cy < 0 || lx == s.rows || cy == s.columns) {
    somme = somme + (0 * M.get(l+q,col+q));
    }else{
    somme = somme + s.get(lx,cy) * M.get(l+q,col+q);
      }
    } 
  }
  return somme;    
}


// var A = new Matrix([1,1,2,5,4]);
// changement de l'image en matrice 2 dimension 

// var bx = [1,5,2,3,55,4,45,128,1,25,78,255];
// var bxTrier = tri(bx);
// function rechercherTab(valeur , tab) {
//   var longueurTab = tab.length;
//   console.log("tab", tab);
//   var milieu = parseInt(longueurTab / 2);
//   console.log("milieu", milieu);
//   console.log("tabMilieu", tab[milieu]);
//   if(tab.length == 2) {
//     if(tab[0] != valeur || tab[1] != valeur) {   
//       console.log("Non trouver!");
//       return valeur;
//     }
//   }else if(tab[milieu] == valeur) {
//     console.log("trouver!");
//     return valeur;
//   } else if(tab[milieu] > valeur) {
//     var tabinf = tab.slice(0,milieu + 1);
//     console.log(tabinf);
//     return rechercherTab(valeur, tabinf);
//   } else{
//     var tabsup = tab.slice(milieu, tab.length);
//     console.log(tabsup);
//     return rechercherTab(valeur, tabsup);
//   } 
// }

// function fusion(gauche, droite){
  
//   var tab = [], l = 0, r = 0;

//   while (l < gauche.length && r < droite.length){
//       if (gauche[l] < droite[r]){
//           tab.push(gauche[l++]);
//       } else {
//           tab.push(droite[r++]);
//       }
//   }
//   return tab.concat(gauche.slice(l)).concat(droite.slice(r));
// }

// function tri(tab){

//   if (tab.length < 2) {
//       return tab;
//   }

//   var mid = Math.floor(tab.length / 2),
//       droite = tab.slice(mid),
//       gauche = tab.slice(0, mid),
//       p = fusion(tri(gauche), tri(droite));
  
//   p.unshift(0, tab.length);
//   tab.splice.apply(tab, p);
//   return tab;
// }

// console.log(rechercherTab(257, bxTrier));
// var tab = [];
// tab.push([1,2]);
// tab.push([1,3]);
// x = 1,
// y = 1112;
// z = x + (y / 10000);
// tab.push(z);
// console.log(tab);

  // var matrice2d = [];
  // var matriceFinal = [];
  
  // for(var b = 0; b < bx.length ; b+=4 ){
  //   matrice2d.push([bx[b],bx[b+1],bx[b+2],bx[b+3]]);
  // }
  // var mt = new Matrix(matrice2d);
  // //console.log(mt);
  // for(var i = 0; i < mt.rows ; i++) { // ligne
  //   for(var c = 0; c < mt.columns ; c++){ // colonne
  //     matriceFinal.push(mt.get(i,c));
  //   }
  // }
  // console.log(matriceFinal);
  // const afficher = document.getElementById("kol").innerText;
  // alert(afficher);

// let a = [1,-2,5,4,3,6];
// a.sort();
// console.log(a);

// const aT = A.transpose();
// aT.addRow(2, [5,4])
// //console.log(aT)

// var x = aT.clone();
// // console.log(x);
// aT.set(0,0, 5);
// //console.log(aT)
// //console.log(aT.get(0,0));
// var q = parseInt(5/2)
//console.log(q);
// const addition = Matrix.add(A, B);
// console.log(addition);
// // let v = [
//     [1,3,34] , 
//     [2,3,4]
// ]

// for ( var j = -1 ; j  < 2 ; j++){
// // //     for ( var i = 0 ; i < v[0].length ; i++ ){
// // //         console.log(v[j][i])
// // //     } 
//     console.log(j);
// }

// v.forEach(item => console.log(item[1]),
//                 console.log(v.length)
// // );
// const matrix = Matrix.ones(5,5);
// const mzeros = Matrix.zeros(1,3)
// console.log(matrix , mzeros)


// //recherche par dichotomie , il faut que le tableau soit trie par ordre croissant ou decroissant
// function rechDichotomie (ligne, col, debut, fin, tab = []) {
//   var milieu = parseInt((debut + fin) / 2);
//   if (debut == fin) {
//     if (tab[debut] == ligne + (col/10000)) {
//       return true;
//     } else if (tab[fin] == ligne + (col/10000)){
//       return true;
//     } else {
//       return col;
//     }
//   }
//   if(tab[milieu] == ligne + (col/10000)) {
//     console.log("1")
//     return true;
//   }else if(tab[milieu] < ligne + (col/10000)) {
//     console.log("ff");
//     return rechDichotomie(ligne, col, milieu + 1 , fin, tab);
//   }else if(tab[milieu] > ligne + (col/10000)){
//     console.log("hh");
//     return rechDichotomie(ligne, col,  debut, milieu - 1, tab);
//   }

// }

// var tab = [1.0001, 1.0002, 1.0003, 1.0004, 1.0005, 1.0006, 1.0007, 1.0008, 1.0009, 1.0010, 1.0011, 1.0012, 1.0013, 1.0014];
// var fin = tab.length - 1;
// console.log(rechDichotomie(1, 2, 0, fin, tab));