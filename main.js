'use strict'

const { app, BrowserWindow, ipcMain, screen, dialog, Menu } = require('electron');
const db = require('./db/stores/project.store');
const Path = require('path');
const menu = require('./menu');

global.db = db;

const url = require('url'),
    path = require('path'),
    fs = require('fs');

let mainWindow;

function bootstrap() {
    const { width, height } = screen.getPrimaryDisplay().workAreaSize;
    mainWindow = new BrowserWindow({
        width: parseInt(75 * width / 100),
        height: parseInt(75 * height / 100),
        minWidth: parseInt(75 * width / 100),
        minHeight: parseInt(75 * height / 100),
        title: "Picado - Démarrer | Editeur photo",
        icon: path.join(__dirname, './src/assets/img/picado-favicon.png'),
        webPreferences: {
            enableRemoteModule: true,
            nodeIntegration: true,
            preload: path.join(__dirname, 'preload.js')
        },
        frame: false
    });

    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, './src/app/app.html'),
        protocol: 'file',
        slashes: true
    }));
}

app.on('window-all-closed', () => {
    if (process.platform !== 'darwin') {
        app.quit()
    }
});

app.on('activate', () => {
    if (BrowserWindow.getAllWindows().length === 0) {
        bootstrap();
    }
});

ipcMain.on('picado:force-reload', function(e, args) {
    BrowserWindow.getFocusedWindow().reload();
});

ipcMain.on('display-app-menu', function(e, args) {
    if (process.platform !== 'darwin' && mainWindow) {
        menu.popup({
            window: mainWindow,
            x: args.x,
            y: args.y
        });
    }
});

ipcMain.on('project:undo-action', function(e) {
    console.log("Undo");
});

ipcMain.on('project:redo-action', function(e) {
    console.log("Redo");
});

ipcMain.on('project:new-project', function(event) {
    dialog.showOpenDialog({
        properties: ['openFile'],
        filters: [{
            name: 'Fichiers Image',
            extensions: ['jpg', 'png', 'tif']
        }]
    }).then((files) => {
        if (files) {
            // const sliptedUrl = filePaths[0].split('\\');
            // const fileName = sliptedUrl[sliptedUrl.length - 1];
            // fs.copyFile(files.filePaths[0], path.join(__dirname, './src/temps/project', fileName, () => {
            //     console.log("Ojey");
            //     event.sender.send('project:project-created', files);
            // }));
            event.sender.send('project:project-created', files);
        }
    });
});

ipcMain.on('project:get-export-path', function(event, args) {
    const name = args.name,
        filters = args.filters;
    const dir = app.getPath('documents') + '/Picado';
    if (!fs.existsSync(dir)) {
        fs.mkdirSync(dir);
    }
    dialog.showSaveDialog({
        title: 'Choisissez l\'emplacemenent du fichier :',
        buttonLabel: 'Exporter',
        defaultPath: dir + '/' + name,
        filters: [filters],
        properties: [],
    }).then((result) => {
        if (!result.canceled) {
            event.sender.send('project:give-export-path', result.filePath);
        }
    }).catch((err) => {
        console.error(err);
    });
});

ipcMain.on('project:export', function(event, args) {
    const url = args.url,
        path = args.path;

    var base64Data = url.replace(/^data:image\/png;base64,/, "");
    fs.writeFile(path, base64Data, 'base64', function(err) {
        if (!err) {
            event.sender.send('project:success-export', path);
        } else throw err;
    });
});

ipcMain.on('project:save-thumbnail', function(event, args) {
    const url = args.url,
        path = Path.join(__dirname, './src/temps/thumbnails/', args.name);
    console.log(path);
});

ipcMain.on('window:set-title-request', function(event, args) {
    mainWindow.setTitle(args.title);
});

app.whenReady().then(bootstrap);