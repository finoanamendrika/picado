const { Menu, ipcRenderer } = require('electron');
const { getAccelerator } = require('./src/helpers/utils');

const isMac = process.platform === "darwin";

const template = [{
        label: 'Fichier',
        submenu: [{
                label: 'Nouveau',
                accelerator: getAccelerator('N'),
                submenu: [{
                        label: 'Projet vide'
                    },
                    {
                        label: 'Créer à partir d\'une modèle'
                    }
                ]
            },
            {
                label: 'Ouvrir',
                accelerator: getAccelerator('O')
            },
            {
                label: 'Importer'
            },
            {
                label: 'Exporter'
            },
            {
                label: 'Quitter',
                accelerator: getAccelerator('Q'),
                role: isMac ? "close" : "quit"
            }
        ]
    },
    {
        label: 'Edition',
        submenu: [{
                label: 'Undo',
                accelerator: getAccelerator('Z'),
                click: () => {
                    ipcRenderer.send('project:undo-action', true);
                }
            },
            {
                label: 'Redo',
                accelerator: getAccelerator('Shift+Z'),
                click: () => {
                    ipcRenderer.send('project:redo-action', true);
                }
            }
        ]
    },
    {
        label: 'Affichage'
    },
];

if (process.platform === 'darwin') {
    template.unshift({});
}

if (process.env.NODE_ENV !== 'production') {
    template.push({
        label: 'Développement',
        submenu: [{
                label: 'Basculer en mode développement',
                accelerator: getAccelerator('I'),
                click(item, focusedWindow) {
                    focusedWindow.toggleDevTools();
                }
            },
            {
                label: 'Recharger',
                role: 'reload'
            }
        ]
    })
}

const menu = Menu.buildFromTemplate(template);
Menu.setApplicationMenu(menu);

module.exports = menu;