function getAccelerator(key) {
    let command = process.platform === 'darwin' ? 'Command+' : 'Ctrl+';
    return command + key;
}

module.exports = {
    getAccelerator
};