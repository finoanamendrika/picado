class PicadoEventEmitter {

    constructor() {
        this._events = {};
    }

    on(name, listener) {
        if (!this._events[name]) {
            this._events[name] = [];
        }

        this._events[name].push(listener);
    }

    removeListener(name, listenerToRemove) {
        if (!this._events[name]) {
            throw new Error(`Can't remove a listener. Event ${name} doesn't exist.`);
        }

        const filterListeners = (listener) =>
            listener !== listenerToRemove;
        this._events[name] = this._events[name].filter(filterListeners);
    }

    emit(name, data) {
        if (!this._events[name]) {
            throw new Error(`Can't emit a listener. Event ${name} doesn't exist.`);
        }

        const fireCallbacks = (callback) => {
            callback(data)
        };

        this._events[name].forEach(fireCallbacks);
    }
}

module.exports = {
    PicadoEventEmitter
}