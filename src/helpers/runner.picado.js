class Runner {
    constructor(task = () => {}) {
        this.task = task;
    }

    run = async() => {
        let begin = new Date();
        let result = this.task.call();
        let end = new Date();
        return {
            result: result,
            executionTime: `${end - begin }ms`
        }
    }
}

module.exports = {
    Runner
}