class PicadoModal {
    modal = document.getElementById("picado-modal");
    closeModal = document.getElementById("picado-modal-close");
    modalActions = document.getElementById("picado-modal-actions");
    modalIcon = document.getElementById("picado-modal-icon");
    modalTitle = document.getElementById("picado-modal-title");
    modalMessage = document.getElementById("picado-modal-message");
    picadoIcon = document.getElementById("picado-modal-picado");
    styles = {
        input: {
            text: {
                padding: '10px 15px 10px 15px',
                border: '1px solid #eee',
                width: '100%',
                marginTop: '10px',
                fontFamily: 'Open Sans Regular',
                fontSize: '.75rem',
                outline: 'none',
            }
        }
    }

    constructor(type = 'warning' | 'danger' | 'success' | 'info', title = "", message = "", actions = null, forms = null) {
        this.type = type;
        this.title = title;
        this.message = message;
        this.sharedElement = {
            input: [{ elementRef: null }]
        }

        let colorScheme;
        switch (this.type) {
            case 'warning':
                {
                    colorScheme = '#F1C40F';
                }
                break;
            case 'danger':
                {
                    colorScheme = '#FF7C6C';
                }
                break;
            case 'success':
                {
                    colorScheme = '#3EDC81';
                }
                break;
            case 'info':
                {
                    colorScheme = '#22A7F0';
                }
                break;
            default:
                {
                    this.modalIcon.style.display = 'none';
                    this.picadoIcon.style.display = 'block';
                }
                break;
        }

        this.modalIcon.style.color = colorScheme;
        this.modalTitle.textContent = this.title;
        this.modalMessage.textContent = this.message;

        this.closeModal.addEventListener("click", (e) => {
            e.preventDefault();
            this.close();
        });

        actions = [...actions, {
            name: "Annuler",
            secure: false,
            do: () => {
                this.close();
            }
        }];

        this.modalActions.innerHTML = '';

        if (actions) {
            actions.map((action, i) => {
                var button = document.createElement("button");
                button.classList.add("action");
                if (action.secure == true) {
                    button.classList.add("do-it");
                }
                button.innerText = action.name;
                button.addEventListener("click", action.do);
                this.modalActions.append(button);
            });
        }
        if (forms && forms.length > 0) {
            forms.map((form, i) => {
                let input = document.createElement('input');
                input.type = form.type;
                input.placeholder = form.placeholder;
                input.style.padding = this.styles.input.text.padding;
                input.style.border = this.styles.input.text.border;
                input.style.width = this.styles.input.text.width;
                input.style.marginTop = this.styles.input.text.marginTop;
                input.style.fontFamily = this.styles.input.text.fontFamily;
                input.style.fontSize = this.styles.input.text.fontSize;
                input.style.outline = this.styles.input.text.outline;
                this.modalMessage.append(input);
                this.sharedElement.input[i].elementRef = input;
            });
        }
    }

    show() {
        this.modal.classList.remove("close-modal");
        this.modal.classList.add("open-modal");
    }

    close() {
        this.modal.classList.remove("open-modal");
        this.modal.classList.add("close-modal");
    }
}

module.exports = {
    PicadoModal
}