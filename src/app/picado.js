const { Matrix } = require('ml-matrix');

const { PicadoModal } = require('../helpers/modal.picado');

Array.prototype.getProps = function() {
    return {
        rows: this.length,
        columns: this.length > 0 && this[0].length > 0 ? this[0].length : 0
    }
}

Array.prototype.getAtPos = function(row, col) {
    return this[row][col] && this[row][col].length > 0 ? this[row][col] : null;
}

Array.prototype.getAtLine = function(line) {
    return this[line] ? this[line] : undefined;
}

Array.prototype.setAtPos = function(row, col, value) {
    if (this[row][col]) {
        this[row][col] = value;
    }
}

Array.prototype.multByValue = function(value) {
    for (let i = 0; i < this.length; i++) {
        this[i] = this[i] * value;
    }
    return this;
}


function separateLayer(data = []) {
    var R = [],
        V = [],
        B = [],
        A = [];
    for (let i = 0; i < data.length; i += 4) {
        R.push(data[i]);
        V.push(data[i + 1]);
        B.push(data[i + 2]);
        A.push(data[i + 3]);
    }
    return {
        r: R,
        v: V,
        b: B,
        a: A
    };
}

function cumulatedHisto(data = []) {
    return getHisto(data);
}

function getCluster(data = []) {
    /* Classification des niveaux */
    var cluster = [];
    for (let i = 0; i < data.length; i++) {
        let index = cluster.findIndex(function(e) { return e == data[i]; });
        if (index == -1) {
            cluster.push(data[i]);
        }
    }
    cluster = cluster.sort(function(a, b) { return a - b });

    for (let i = 0; i <= 255; i++) {
        let index = cluster.findIndex(function(e) { return e == i; });
        if (index == -1) {
            cluster.push(i);
        }
    }

    cluster = cluster.sort(function(a, b) { return a - b });

    return cluster;
}

async function getHisto(data = []) {
    return new Promise((sucess, error) => {
        let cluster = { r: [], v: [], b: [], a: [] };
        let layers = separateLayer(data);
        cluster.r = getCluster(layers.r);
        cluster.v = getCluster(layers.v);
        cluster.b = getCluster(layers.b);
        cluster.a = getCluster(layers.a);

        let result = {
            r: calculateHistoPerLayer(layers.r, cluster.r),
            v: calculateHistoPerLayer(layers.v, cluster.v),
            b: calculateHistoPerLayer(layers.b, cluster.b),
            a: calculateHistoPerLayer(layers.a, cluster.a),
        }

        sucess(result);
        error('[ERROR][getHisto] : Can\'t getHisto on image.');
    });
}

async function binarisation(data = []) {
    return new Promise((success, error) => {
        greyScale(data).then((greyScaledArray) => {
            getHisto(greyScaledArray).then((histograms) => {
                let result = greyScaledArray;
                let { r, v, b } = histograms;
                let seuil = {
                    r: getSeuil(r),
                    v: getSeuil(v),
                    b: getSeuil(b),
                }
                for (let i = 0; i < result.length; i++) {
                    result[i] = result[i] < seuil.r ? 0 : 255;
                    result[i + 1] = result[i + 1] < seuil.v ? 0 : 255;
                    result[i + 2] = result[i + 2] < seuil.b ? 0 : 255;
                }
                success(result);
                error(new Error('[ERROR][Binarisation] : Error when binarizing image.'));
            });
        }, (error) => {
            throw error;
        });
    });
}

async function etalementDynamique(data = []) {
    return new Promise((success, error) => {
        var max = 200;
        var min = 12;
        for (let i = 0; i < data.length; i += 4) {
            data[i] = (255 * (data[i] - min)) / (max - min);
            data[i + 1] = (255 * (data[i + 1] - min)) / (max - min);
            data[i + 2] = (255 * (data[i + 2] - min)) / (max - min);
        }
        success(data);
        error(new Error('[ERROR][etalementDynamique] : Can\'t add transformation etalementDynamique on image.'));
    });
}

function calculateHistoPerLayer(layer = [], cluster = []) {
    var histogram = new Array(cluster.length);
    for (let i = 0; i < histogram.length; i++) {
        histogram[i] = 0;
    }
    for (let i = 0; i < layer.length; i++) {
        let index = cluster.findIndex(function(e) { return e == layer[i]; });
        histogram[index]++;
    }
    return histogram;
}

function convertTo2D(data = []) {
    var matrix2D = [];
    var len = data.length;
    for (let i = 0; i < len; i += 4) {
        matrix2D.push([data[i], data[i + 1], data[i + 2], data[i + 3]]);
    }
    return new Matrix(matrix2D);
}

function conversion(x, y, width) {
    return (y * width + x) * 4;
}

function invert(data = []) {
    for (var i = 0; i < data.length; i += 4) {
        data[i] = 255 - data[i];
        data[i + 1] = 255 - data[i + 1];
        data[i + 2] = 255 - data[i + 2];
    }
    return data;
}

async function greyScale(data = []) {
    return new Promise((success, error) => {
        for (var i = 0; i < data.length; i += 4) {
            let grey = (data[i] + data[i + 1] + data[i + 2]) / 3;
            data[i] = grey;
            data[i + 1] = grey;
            data[i + 2] = grey;
        }
        success(data);
        error(new Error('[ERROR][greyScale] : Can\'t greyScale image.'));
    });
}

function addBlue(data = [], width, height) {
    return new Promise((success, error) => {
        for (let i = 0; i < data.length; i += 4) {
            data[i + 2] = clamp(data[i + 2] + 50);
        }
        success(data);
        error(new Error('[ERROR][addBlue] : Can\'t add blue value in image.'));
    });
}

function clamp(value) {
    return Math.max(0, Math.min(Math.floor(value), 255));
}

function flagging(data = [], width, height) {
    width = parseInt(width);
    height = parseInt(height);
    return new Promise((success, error) => {
        const offset = parseInt(width / 3);
        for (let section = 1; section <= 3; section++) {
            for (let i = 0; i < (offset * section); i++) {
                for (let j = 0; j < height; j++) {
                    let index = ((i + j * width) * 4) + (section - 1);
                    data[index] = clamp(data[index] + 50);
                }
            }
        }
        success(data);
        error(new Error('[ERROR][flagging] : Can\'t add flag effect in image.'));
    });
}

function rotate(data = [], width, height, angle = 0) {
    return new Promise((success, error) => {
        let result = new Array(data.length);
        for (let i = 0; i < result.length; i += 4) {
            result[i] = 0;
            result[i + 1] = 0;
            result[i + 2] = 0;
            result[i + 3] = 255;
        }
        var posSrc, posDest;
        angle = angle / (Math.PI * 180);
        let cos = Math.cos(angle).toFixed(2),
            sin = Math.sin(angle).toFixed(2);
        let center = { X: Math.round(width / 2), Y: Math.round(width / 2) };
        let centerPos = conversion(center.X, center.Y, width);
        for (let y = 0; y < width; y++) {
            for (let x = 0; x < height; x++) {
                posSrc = conversion(x, y, width);
                posDest = Math.abs(centerPos - conversion(Math.round((x * cos) + (y * (sin * (-1)))), Math.round((x * sin) + (y * cos)), width));
                for (let i = 0; i < 4; i++) {
                    result[posDest + i] = data[posSrc + i];
                }
            }
        }
        success(result);
    });
}

function horizontalSymetric(data = [], width, height) {
    let result = [];
    let i = data.length - 1;
    while (i > 0) {
        result.push(data[i - 3]);
        result.push(data[i - 2]);
        result.push(data[i - 1]);
        result.push(data[i]);
        i -= 4;
    }
    return result;
}

function verticalSymetric(data = [], width) {
    return new Promise((success, error) => {
        getImageArray(data, width, 4).then((matrice2D) => {
            let result = matrice2D.clone();
            for (let i = 0; i < matrice2D.rows; i++) {
                let j = matrice2D.columns - 1;
                let k = 0;
                while (j > 0) {
                    result.set(i, k, matrice2D.get(i, j - 3));
                    result.set(i, k + 1, matrice2D.get(i, j - 2));
                    result.set(i, k + 2, matrice2D.get(i, j - 1));
                    result.set(i, k + 3, matrice2D.get(i, j));
                    k += 4;
                    j -= 4;
                }
            }
            result = result.to1DArray();
            success(result);
            error(new Error('[ERROR][verticalSymetric] : Can\'t add verticalSymetric on image.'));
        });
    });
}

function filterRed(data = []) {
    for (i = 0; i < data.length; i += 4) {
        data[i + 1] = 0;
        data[i + 2] = 0;
    }
    return data;
}

function filterGreen(data = []) {
    for (i = 0; i < data.length; i += 4) {
        data[i] = 0;
        data[i + 2] = 0;
    }
    return data;
}

function filterBlue(data = []) {
    for (i = 0; i < data.length; i += 4) {
        data[i] = 0;
        data[i + 1] = 0;
    }
    return data;
}

function getRedColors(data = []) {
    let ratio = 1 / 256;
    for (i = 0; i < data.length; i += 4) {}
    return data;
}

function pixelize(data = []) {
    var pixelized = [];
    for (let i = 0; i < data.length; i += 4) {
        let pixel = [data[i], data[i + 1], data[i + 2], data[i + 3]];
        pixelized.push(pixel);
    }
    return pixelized;
}

function makeLinesOfPixels(data = [], width, height) {
    var pixelized = pixelize(data);
    var lines = Matrix.zeros(height, 1);
    var tmp = [];
    var currentStep = 0;
    for (let i = 0; i <= pixelized.length; i++) {
        let step = parseInt(i / width);
        tmp.push(pixelized[i]);
        if (step > currentStep) {
            lines.set(step, 0, tmp);
            currentStep = step;
            tmp = [];
        }
    }
    return lines;
}

function getImageArray(data = [], width, nrbPxl) {
    return new Promise((success, error) => {
        width = parseInt(width);
        var result = [];
        let step = width * nrbPxl;
        for (let i = 0; i < data.length; i += step) {
            let start = i;
            let end = i + step;
            result.push(data.slice(start, end));
        }
        success(new Matrix(result));
        error(new Error('[ERROR][getImageArray] : Can\'t convert image to Matrix.'));
    });
}

function pointLinearFilter(x, y, data = new Matrix([]), mask = new Matrix()) {
    const q = parseInt(Math.sqrt(mask.size) / 2);
    let somme = 0.0;
    for (let col = -parseInt(mask.columns / 2); col < (parseInt(mask.columns / 2) + 1); col++) {
        for (let l = -parseInt(mask.columns / 2); l < (parseInt(mask.columns / 2) + 1); l++) {
            lx = x - l; // index de lignecxcxxxxxxxxxxxxxxxxxx     
            cy = y - col; // index de la colonne
            if (lx < 0 || cy < 0 || lx == data.rows || cy == data.columns) {
                somme = somme + (0 * mask.get(l + q, col + q));
            } else {
                somme = somme + data.get(lx, cy) * mask.get(l + q, col + q);
            }
        }
    }
    return somme;
}


function getArrayProps(array = new Array()) {
    return {
        rows: array.length,
        columns: array.length > 0 && array[0].length > 0 ? array[0].length : 0
    }
}

async function Filtrage_lineaire_image(matrix = new Matrix([]), masque = new Matrix([])) {
    return new Promise((success, error) => {
        let mx = matrix.clone();
        for (let y = 0; y < matrix.columns; y++) {
            for (let x = 0; x < matrix.rows; x++) {
                mx.set(x, y, Filtrage_lineaire_point(x, y, matrix, masque));
            }
        }
        success(mx);
        error(new Error('[ERROR][Filtrage_lineaire_image] : Can\'t Filtrage_lineaire_image image.'));
    })
}

function Filtrage_lineaire_point(x, y, s = new Matrix([]), M = new Matrix([])) {
    const q = parseInt(Math.sqrt(M.size) / 2);
    let somme = 0.0;
    for (let col = -parseInt(M.columns / 2); col < (parseInt(M.columns / 2) + 1); col++) {
        for (let l = -parseInt(M.columns / 2); l < (parseInt(M.columns / 2) + 1); l++) {
            let lx = x - l; // index de lignecxcxxxxxxxxxxxxxxxxxx     
            let cy = y - col; // index de la colonne
            if (lx < 0 || cy < 0 || lx == s.rows || cy == s.columns) {
                break;
            } else {
                somme = somme + s.get(lx, cy) * M.get(l + q, col + q);
            }
        }
    }
    let result = somme < 0 ? 0 : somme > 255 ? 255 : somme;
    return result;
}

async function linearFilter(data = [], width, filter) {
    return new Promise((success, error) => {
        let mask;
        switch (filter) {
            case 'lissage':
                mask = [
                    [1 / 9, 1 / 9, 1 / 9],
                    [1 / 9, 1 / 9, 1 / 9],
                    [1 / 9, 1 / 9, 1 / 9]
                ]
                break;
            case 'convolution':
                mask = [
                    [-5, 0, 1],
                    [-1, -1, -5],
                    [8, -1, 3]
                ]
                break;
            case 'gaussien3x3':
                mask = [
                    [1 / 16, 1 / 8, 1 / 16],
                    [1 / 8, 1 / 4, 1 / 8],
                    [1 / 16, 1 / 8, 1 / 16]
                ]
                break;
            case 'rehausseur':
                mask = [
                    [0, -1, 0],
                    [-1, 5, -1],
                    [0, -1, 0]
                ]
                break;
            case 'accentuation':
                mask = [
                    [0, -0.5, 0],
                    [-0.5, 3, -0.5],
                    [0, -0.5, 0]
                ]
                break;
            case 'laplacien':
                mask = [
                    [0, 1, 0],
                    [1, -4, 1],
                    [0, 1, 0]
                ]
                break;
            case 'sobel1x':
                mask = [
                    [-1, -2, -1],
                    [0, 0, 0],
                    [1, 2, 1]
                ]
                break;
            case 'sobel2x':
                mask = [
                    [-1, 0, 1],
                    [-2, 0, 2],
                    [-1, 0, 1]
                ]
                break;
            default:
                mask = [
                    [1, 1, 1],
                    [1, 1, 1],
                    [1, 1, 1]
                ]
                break;
        }
        getImageArray(data, width, 4).then((matrixResult) => {
            var __data = matrixResult;
            mask = new Matrix(mask);

            var rouge = Matrix.zeros(__data.rows, (__data.columns) / 4);
            var vert = Matrix.zeros(__data.rows, (__data.columns) / 4);
            var bleu = Matrix.zeros(__data.rows, (__data.columns) / 4);

            for (let ligne = 0; ligne < __data.rows; ligne++) {
                for (let col = 0; col < __data.columns; col += 4) {
                    rouge.set(ligne, parseInt(col / 4), __data.get(ligne, col));
                    vert.set(ligne, parseInt(col / 4), __data.get(ligne, col + 1));
                    bleu.set(ligne, parseInt(col / 4), __data.get(ligne, col + 2));
                }
            }
            Filtrage_lineaire_image(rouge, mask).then((rougeResult) => {
                var filtreRouge = rougeResult;
                Filtrage_lineaire_image(vert, mask).then((vertResult) => {
                    var filtreVert = vertResult;
                    Filtrage_lineaire_image(bleu, mask).then((bleuResult) => {
                        var filtreBleu = bleuResult;

                        for (let ligne = 0; ligne < __data.rows; ligne++) {
                            for (let col = 0; col < __data.columns; col += 4) {
                                __data.set(ligne, col, filtreRouge.get(ligne, parseInt(col / 4)));
                                __data.set(ligne, col + 1, filtreVert.get(ligne, parseInt(col / 4)));
                                __data.set(ligne, col + 2, filtreBleu.get(ligne, parseInt(col / 4)));
                            }
                        }
                        var result = [];
                        for (let ligne = 0; ligne < __data.rows; ligne++) {
                            for (let col = 0; col < __data.columns; col++) {
                                result.push(__data.get(ligne, col));
                            }
                        }
                        success(result);
                        error(new Error('[ERROR][linearFilter] : Can\'t linearFilter image'));

                    }, (error) => {
                        throw error;
                    });
                }, (error) => {
                    throw error;
                })
            }, (error) => {
                throw error;
            });
        }, (error) => {
            throw error;
        });
    })
}

function getSeuil(histogram = []) {
    var otsu = 0;
    var omega1, mu1, omega2, mu2;
    var sigma, maxsigma = 0;
    for (let t = 0; t < histogram.length; t++) {
        omega1 = 0;
        mu1 = 0;
        for (let i = 0; i <= t; i++) {
            omega1 += histogram[i];
            mu1 += histogram[i] * i;
        }
        mu1 /= omega1;

        omega2 = 0;
        mu2 = 0;
        for (let i = t + 1; i < histogram.length; i++) {
            omega2 += histogram[i];
            mu2 += histogram[i] * i;
        }
        mu2 /= omega2;

        sigma = omega1 * omega2 * Math.pow(mu1 - mu2, 2);
        if (sigma > maxsigma) {
            otsu = t;
            maxsigma = sigma;
        }
    }
    return otsu;
}

/* Tortue de Papert */

var matriceClone = [];
const Direction = {
    Left: "left",
    Right: "right",
    Top: "top",
    Down: "down"
}
var currentDirection = Direction.Right;

function getDirection(lineCurrent, colCurrent, newLine, newColumn) {
    var newDirection = "";
    if (newLine < lineCurrent && newColumn == colCurrent) {
        newDirection = Direction.Top;
    } else if (newLine > lineCurrent && newColumn == colCurrent) {
        newDirection = Direction.Down;
    } else if (newLine == lineCurrent && newColumn < colCurrent) {
        newDirection = Direction.Left;
    } else if (newLine == lineCurrent && newColumn > colCurrent) {
        newDirection = Direction.Right;
    }
    return newDirection;
}

function turnLeft(lineCurrent, colCurrent, direction) {
    var newLine = 0,
        newColumn = 0;
    switch (direction) {
        case Direction.Right:
            newLine = lineCurrent - 1;
            newColumn = colCurrent;
            break;
        case Direction.Left:
            newLine = lineCurrent + 1;
            newColumn = colCurrent;
            break;
        case Direction.Top:
            newLine = lineCurrent;
            newColumn = colCurrent - 1;
            break;
        case Direction.Down:
            newLine = lineCurrent;
            newColumn = colCurrent + 1;
            break;
    }
    var newPoint = {
        newLine: newLine,
        newColumn: newColumn
    }
    return newPoint;
}

function turnRight(lineCurrent, colCurrent, direction) {
    var newLine = 0,
        newColumn = 0;
    switch (direction) {
        case Direction.Right:
            newLine = lineCurrent + 1;
            newColumn = colCurrent;
            break;
        case Direction.Left:
            newLine = lineCurrent - 1;
            newColumn = colCurrent;
            break;
        case Direction.Top:
            newLine = lineCurrent;
            newColumn = colCurrent + 1;
            break;
        case Direction.Down:
            newLine = lineCurrent;
            newColumn = colCurrent - 1;
            break;
    }
    var newPoint = {
        newLine: newLine,
        newColumn: newColumn
    }
    return newPoint;
}

function contouring(data = [], width) {
    return new Promise((success, error) => {
        binarisation(data).then((binarizedImage) => {
            rgbaToRgb(binarizedImage).then((rgbArray) => {
                getImageArray(rgbArray, width, 3).then((matrice) => {
                    var contourizedMatrice = Contour(matrice);
                    console.log(contourizedMatrice);
                    var contourizedRGBArray = contourizedMatrice.to1DArray();
                    rgbToRgba(contourizedRGBArray).then((result) => {
                        success(result);
                        error(new Error('[ERROR][contouring] : Can\'t contouring image.'));
                    }, (error) => { throw error; });
                }, (error) => { throw error; });
            }, (error) => { throw error; });
        }, (error) => { throw error; });
    });
}

function Contour(matrice = new Matrix([])) {
    var form = 0;
    var back = 255;
    var runTurtle = false;
    for (let i = 0; i < matrice.columns; i++) {
        matrice.set(0, i, back);
    }
    for (let index = 0; index < matrice.columns; index++) {
        matrice.set(matrice.rows - 1, index, back);
    }
    for (let i = 0; i < matrice.rows; i++) {
        matrice.set(i, 0, back);
    }
    for (let i = 0; i < matrice.rows; i++) {
        matrice.set(i, matrice.rows - 1, back);
    }

    matriceClone = new Array(matrice.rows);

    for (let ligne = 0; ligne < matrice.rows; ligne++) {
        let tmp = [];
        for (let col = 0; col < matrice.columns; col++) {
            tmp.push(back);
        }
        matriceClone[ligne] = tmp;
    }

    matriceClone = new Matrix(matriceClone);

    for (let ligne = 0; ligne < matrice.rows; ligne++) {
        // if (ligne == matrice.rows - 1) {
        //     return matriceClone;
        // }
        for (let col = 0; col < matrice.columns; col++) {
            if (matrice.get(ligne, col) == form) {
                runTurtle = true;
                if (matriceClone.get(ligne, col) == form) {
                    runTurtle = false;
                } else if (matrice.get(ligne, col - 1) == form && matrice.get(ligne, col + 1) == form && matriceClone.get(ligne, col) == back) {
                    runTurtle = false;
                    col += 2;
                }
            }
            const beginLine = ligne,
                beginColumn = col;
            while (runTurtle == true) {
                if (matrice.get(ligne, col) == form) {
                    matriceClone.set(ligne, col, form);
                    var point = turnLeft(ligne, col, currentDirection);
                    currentDirection = getDirection(ligne, col, point.newLine, point.newColumn);
                    ligne = point.newLine;
                    col = point.newColumn;
                } else if (matrice.get(ligne, col) == back) {
                    var point = turnRight(ligne, col, currentDirection);
                    currentDirection = getDirection(ligne, col, point.newLine, point.newColumn);
                    ligne = point.newLine;
                    col = point.newColumn;
                }
                if (beginLine == ligne && beginColumn == col) {
                    runTurtle = false;
                    currentDirection = Direction.Right;
                }
            }
        }
    }
    return matriceClone;
}

function rgbaToRgb(data = []) {
    return new Promise((success, error) => {
        var rgb = [];
        for (let index = 0; index < data.length; index += 4) {
            rgb.push(data[index]);
            rgb.push(data[index + 1]);
            rgb.push(data[index + 2]);
        }
        success(rgb);
        error(new Error('[ERROR][rgbaToRgb] : Can\'t convert RGBA to RGB.'));
    });
}

function rgbToRgba(data = []) {
    return new Promise((success, error) => {
        var rgba = [];
        for (let index = 0; index < data.length; index += 3) {
            rgba.push(data[index]);
            rgba.push(data[index + 1]);
            rgba.push(data[index + 2]);
            rgba.push(255);
        }
        success(rgba);
        error(new Error('[ERROR][rgbToRgba] : Can\'t covert RGB to RGBA.'));
    });
}

/* Amelioration de l'image */

async function contrast(data = [], ratio) {
    return new Promise((success, error) => {
        ratio = (ratio / 100) + 1;
        var intercept = 128 * (1 - ratio);
        for (let i = 0; i < data.length; i += 4) {
            data[i] = data[i] * ratio + intercept;
            data[i + 1] = data[i + 1] * ratio + intercept;
            data[i + 2] = data[i + 2] * ratio + intercept;
        }
        success(data);
        error('[ERROR][contrast] : Can\'t add contrast on image.');
    });
}

async function saturation(data = [], ratio) {
    return new Promise((success, error) => {
        var sv = ratio / 10;

        var luminanceRouge = 0.3086;
        var luminanceGreen = 0.6094;
        var luminanceBleu = 0.0820;

        var az = (1 - sv) * luminanceRouge + sv;
        var bz = (1 - sv) * luminanceGreen;
        var cz = (1 - sv) * luminanceBleu;
        var dz = (1 - sv) * luminanceRouge;
        var ez = (1 - sv) * luminanceGreen + sv;
        var fz = (1 - sv) * luminanceBleu;
        var gz = (1 - sv) * luminanceRouge;
        var hz = (1 - sv) * luminanceGreen;
        var iz = (1 - sv) * luminanceBleu + sv;

        for (let i = 0; i < data.length; i += 4) {
            var saturationRed = (az * data[i] + bz * data[i + 1] + cz * data[i + 2]);
            var saturationGreen = (dz * data[i] + ez * data[i + 2] + fz * data[i + 2]);
            var saturationBlue = (gz * data[i] + hz * data[i + 2] + iz * data[i + 2]);

            data[i] = saturationRed;
            data[i + 1] = saturationGreen;
            data[i + 2] = saturationBlue;
        }

        success(data);
        error(new Error('[ERROR][saturation] : Can\'t add saturation on image.'));
    });
}

async function limunosite(data = [], ratio) {
    return new Promise((success, error) => {
        ratio = parseInt(ratio);
        for (let i = 0; i < data.length; i += 4) {
            data[i] += ratio;
            data[i + 1] += ratio;
            data[i + 2] += ratio;
        }
        success(data);
        error(new Error('[ERROR][limunosite]: Can\'t add luminosity to image.'));
    });
}

/* Erosion */

function erosion(x, y, matrice = new Matrix([]), masque = new Matrix([])) {
    const forground = 0,
        background = 255;
    clone = Matrix.zeros(masque.rows, masque.columns);
    const q = parseInt(Math.sqrt(masque.size) / 2);
    for (var line = -parseInt(masque.columns / 2); line < parseInt(masque.columns / 2) + 1; line++) {
        // console.log("line", line);
        for (let col = -parseInt(masque.columns / 2); col < parseInt(masque.columns / 2) + 1; col++) {
            // console.log("col", col);
            lx = x - line;
            cy = y - col;
            if (lx < 0 || cy < 0 || lx == matrice.rows || cy == matrice.columns) {
                clone.set(-line + q, -col + q, background);
            } else {
                clone.set(-line + q, -col + q, matrice.get(lx, cy));
            }
        }
    }
    for (let ligne = 0; ligne < masque.rows; ligne++) {
        for (let col = 0; col < masque.columns; col++) {
            if (masque.get(ligne, col) != clone.get(ligne, col)) {
                return background;
            }
        }
    }
    return forground;
}

function erosionImage(data = [], width) {
    return new Promise((success, error) => {
        var masque = new Matrix([
            [255, 255, 255],
            [255, 255, 255],
            [255, 255, 255]
        ]);

        binarisation(data).then((binarizedArray) => {
            rgbaToRgb(binarizedArray).then((rgbArray) => {
                getImageArray(rgbArray, width, 3).then((matrix) => {
                    let mx = matrix.clone();
                    for (let y = 0; y < matrix.columns; y++) {
                        for (let x = 0; x < matrix.rows; x++) {
                            mx.set(x, y, erosion(x, y, matrix, masque));
                        }
                    }
                    let rgb1DArray = mx.to1DArray();
                    rgbToRgba(rgb1DArray).then((result) => {
                        success(result);
                        error(new Error('[ERROR][erosionImage] : Can\'t erosionImage image.'));
                    }, (error) => { throw error; });
                }, (error) => { throw error; });
            }, (error) => { throw error; });
        }, (error) => { throw error; });
    }, (error) => { throw error; });
}

/* Dilatation */

function dilatation(x, y, matrice = new Matrix([]), masque = new Matrix([])) {
    const forground = 0,
        background = 255;
    clone = Matrix.zeros(masque.rows, masque.columns);
    const q = parseInt(Math.sqrt(masque.size) / 2);
    for (var line = -parseInt(masque.columns / 2); line < parseInt(masque.columns / 2) + 1; line++) {
        // console.log("line", line);
        for (let col = -parseInt(masque.columns / 2); col < parseInt(masque.columns / 2) + 1; col++) {
            // console.log("col", col);
            lx = x - line;
            cy = y - col;
            if (lx < 0 || cy < 0 || lx == matrice.rows || cy == matrice.columns) {
                clone.set(-line + q, -col + q, background);
            } else {
                clone.set(-line + q, -col + q, matrice.get(lx, cy));
            }
        }
    }
    for (let ligne = 0; ligne < masque.rows; ligne++) {
        for (let col = 0; col < masque.columns; col++) {
            if (masque.get(ligne, col) == clone.get(ligne, col)) {
                return forground;
            }
        }
    }
    return background;
}

function dilatationImage(data = new Matrix([]), width) {
    return new Promise((success, error) => {
        var masque = new Matrix([
            [255, 255, 255],
            [255, 255, 255],
            [255, 255, 255]
        ]);
        binarisation(data).then((binarizedArray) => {
            rgbaToRgb(binarizedArray).then((rgbArray) => {
                getImageArray(rgbArray, width, 3).then((matrix) => {
                    let mx = matrix.clone();
                    for (let y = 0; y < matrix.columns; y++) {
                        for (let x = 0; x < matrix.rows; x++) {
                            mx.set(x, y, dilatation(x, y, matrix, masque));
                        }
                    }
                    let rgb1DArray = mx.to1DArray();
                    rgbToRgba(rgb1DArray).then((result) => {
                        success(result);
                        error(new Error('[ERROR][erosionImage] : Can\'t erosionImage image.'));
                    }, (error) => { throw error; });
                }, (error) => { throw error; });
            }, (error) => { throw error; });
        }, (error) => { throw error; });
    }, (error) => { throw error; });
}

/* Filtrage Median */

function fusion(gauche, droite) {
    var tab = [],
        l = 0,
        r = 0;
    while (l < gauche.length && r < droite.length) {
        if (gauche[l] < droite[r]) {
            tab.push(gauche[l++]);
        } else {
            tab.push(droite[r++]);
        }
    }
    return tab.concat(gauche.slice(l)).concat(droite.slice(r));
}

function tri(tab) {
    if (tab.length < 2) {
        return tab;
    }
    var mid = Math.floor(tab.length / 2),
        droite = tab.slice(mid),
        gauche = tab.slice(0, mid),
        p = fusion(tri(gauche), tri(droite));
    p.unshift(0, tab.length);
    tab.splice.apply(tab, p);
    return tab;
}

function Filtre_median_point(x, y, nbr_impaire, matrice = new Matrix([])) {
    // niveau de gris 
    // trouver les voisinages 
    var tabVoisinage = [];
    const q = parseInt(nbr_impaire / 2);
    for (var line = -q; line < q + 1; line++) {
        // console.log("line", line);
        for (let col = -q; col < q + 1; col++) {
            // console.log("col", col);
            lx = x - line;
            cy = y - col;
            //console.log("lx", lx);
            //console.log("cy",cy);
            if (lx < 0 || cy < 0 || lx >= matrice.rows || cy >= matrice.columns) {
                tabVoisinage.push(0);
            } else {
                tabVoisinage.push(matrice.get(lx, cy));
            }
        }
    }
    //console.log("tabVoisinage", tabVoisinage);
    // tri par ordre croissant
    var tabTrier = tri(tabVoisinage);
    // trouver la valeur mediane des voisinages
    var indexMediane = parseInt((nbr_impaire * nbr_impaire) / 2);
    var median = tabTrier[indexMediane];
    // remplacer le pixel par cette valeur
    return median;
}

function Filtrage_median_image(data = [], width, nbr_impaire) {
    return new Promise((success, errorHandler) => {
        greyScale(data).then((greyScaledArray) => {
            rgbaToRgb(greyScaledArray).then((rgbArray) => {
                getImageArray(rgbArray, width, 3).then((matrice) => {
                    let mx = matrice.clone();
                    for (let y = 0; y < matrice.columns; y++) {
                        for (let x = 0; x < matrice.rows; x++) {
                            mx.set(x, y, Filtre_median_point(x, y, nbr_impaire, matrice));
                        }
                    }
                    let rgb1DArray = matrice.to1DArray();
                    rgbToRgba(rgb1DArray).then((result) => {
                        success(result);
                    }, (error) => { errorHandler(error); });
                }, (error) => { errorHandler(error); });
            }, (error) => { errorHandler(error); });
        }, (error) => { errorHandler(error); });
    });
}

const Picado = {
    algorithms: {
        invert: invert,
        greyScale: greyScale,
        rotate: rotate,
        horizontalSymetric: horizontalSymetric,
        verticalSymetric: verticalSymetric,
        filterRed: filterRed,
        filterGreen: filterGreen,
        filterBlue: filterBlue,
        addBlue: addBlue,
        flagging: flagging,
        binarisation: binarisation,
        getHisto: getHisto,
        getRedColors: getRedColors,
        linearFilter: linearFilter,
        makeLinesOfPixels: makeLinesOfPixels,
        getImageArray: getImageArray,
        contouring: contouring,
        contrast: contrast,
        saturation: saturation,
        limunosite: limunosite,
        erosion: erosionImage,
        dilatation: dilatationImage,
        filterMedian: Filtrage_median_image,
        etalementDynamique: etalementDynamique
    }
}

module.exports = {
    Picado,
    PicadoModal
}