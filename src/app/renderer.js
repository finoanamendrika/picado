const { PicadoModal } = require('./picado');

const { remote } = require('electron');
const dbInstance = remote.getGlobal('db');

window.addEventListener("DOMContentLoaded", () => {
    const openMenuBtn = document.getElementById("openMenu-btn"),
        minimizeBtn = document.getElementById("minimize-btn"),
        maxUnmaxBtn = document.getElementById("max-unmax-btn"),
        closeBtn = document.getElementById("close-btn");

    openMenuBtn.addEventListener("click", e => {
        window.openMenu(e.x, e.y);
    });
    minimizeBtn.addEventListener("click", e => {
        window.minimizeWindow();
    });
    maxUnmaxBtn.addEventListener("click", e => {
        window.maxUnmaxWindow();
    });
    closeBtn.addEventListener("click", e => {
        const modal = new PicadoModal('', 'Quitter Picado', 'Voulez-vous quitter Picado ?', [{
            name: "Quitter",
            secure: true,
            do: () => {
                window.closeWindow();
            }
        }]);
        modal.show();
    });
});