/**
 *  Picado : v0.9.8 (Beta)
 *  Authors: 
 *      RABEHEVITRA Finoana Mendrika,
 *      MANDATIANA Gerson Elvestino,
 *      RABEFIALY RANDRIAMAHEFA Jonathan,
 *      RANDRIANASOLO TOVOARINELINA Nicolas Chabalet
 *  Release: 02 Feb. 2021
 *  Project made with <3 by Picado Team (Master I Info. EMIT Fianarantsoa).
 */

const { ipcRenderer } = require('electron');
const { Picado } = require('./picado');
const { Chart } = require('chart.js');
const { PicadoEventEmitter } = require('../helpers/eventEmitter.picado');

/* Declare Globaly $ as Picado.algorithms  */
let $ = Picado.algorithms;

var picado = {
    version: '0.9.8',
    active: false,
    haveModification: false,
    updatedAt: new Date(),
    project: {
        traitementsQue: [],
    },
    zoomRatio: 100,
    zoomStep: 5,
    width: 0,
    height: 0,
    img: null,
    temporaryCanvas: null,
    startingQueIndex: 0,
    cache: new Array(2),
    toolsPanelIsOpen: false,
    emeliorationsPannelIsActive: false,
    emeliorationsIsModified: false,
    displayProject: localStorage.getItem("PROJECT_DISPLAY_MODE") || 'grid',
    comparaisonProjectMode: 'single',
    transfNeedExtradata: /rotation|horizontalSymetric|linearFilter__lissage|linearFilter__convolution|linearFilter__gaussien3x3|linearFilter__rehausseur|linearFilter__laplacien|addBlue|flagEffect/,
    transfNeedRatio: /contrast|saturation|luminosite/,
    env: 'prod'
}

var imageData,
    originalData,
    timer,
    $elements,
    ameliorationsQue = new Array(3);

let mainCtx;
const projectEventEmitter = new PicadoEventEmitter();

var ViewElementsSelector = (function() {
    function ViewElementsSelector() {}

    ViewElementsSelector.prototype.byId = function(ID) {
        return document.getElementById(ID);
    }

    ViewElementsSelector.prototype.byClassName = function(elemClassName) {
        return document.getElementsByClassName(elemClassName);
    }

    return ViewElementsSelector;
})();

var Bootstrap = (function() {
    var elementsGetter = new ViewElementsSelector();

    function Bootstrap() {
        $elements = {};
    }

    Bootstrap.prototype.init = async function() {
        $elements.appTitle = elementsGetter.byId('appTitle');
        $elements.gettingStartedSection = elementsGetter.byId('gettingStartedSection');
        $elements.editorSection = elementsGetter.byId('editorSection');
        $elements.cursorPosXTracker = elementsGetter.byId('cursorPosXTracker');
        $elements.cursorPosYTracker = elementsGetter.byId('cursorPosYTracker');
        $elements.elapsedTimeLabel = elementsGetter.byId('elapsedTime');
        $elements.saveStateLabel = elementsGetter.byId('save-state-label');
        $elements.zoomValueLabel = elementsGetter.byId('zoomValue');
        $elements.traitementBar = elementsGetter.byId('traitementBar');
        $elements.traitementHistoricsNode = elementsGetter.byId('traitementHistoricsNode');
        $elements.picadAlert = elementsGetter.byId('picado-alert');
        $elements.mainCanvas = elementsGetter.byId('main-display-canvas');
        $elements.createNewProjectBtn = elementsGetter.byId('createNewProjectBtn');
        $elements.createProjectFromURL = elementsGetter.byId('createProjectFromURL');
        $elements.loader = elementsGetter.byId('picado-loader');
        $elements.mainCanvasSecondary = elementsGetter.byId('main-display-secondary-screen');
        $elements.viewerModeContainer = elementsGetter.byId('viewerModeFloatingContainer');
        mainCtx = $elements.mainCanvas.getContext('2d');
        $elements.emeliorationsApplyUndoContainer = elementsGetter.byId('emeliorationsApplyUndoContainer');
        $elements.ranges = elementsGetter.byClassName('range');
    }

    Bootstrap.prototype.initEmitterEvent = function() {
        projectEventEmitter.on('picado:projectLoaded', (data) => {
            picado.active = true;
            $elements.gettingStartedSection.style.display = 'none';
            document.body.classList.add('__dark');
            $elements.editorSection.style.visibility = 'visible';
            $elements.appTitle.textContent = `${ data.projectName } - Picado v${ picado.version }`;
            $elements.viewerModeContainer.classList.toggle('active', true);
            setTimeout(() => {
                picado.active = true;
                $elements.gettingStartedSection.style.display = 'none';
                document.body.classList.add('__dark');
                $elements.editorSection.style.visibility = 'visible';
                projectEventEmitter.emit('picado:toggleLoader', { mode: 'none' });
            }, 1000);
        });
        projectEventEmitter.on('picado:projectCreated', (project) => {
            openCreateProject(project);
        });
        projectEventEmitter.on('picado:projectHaveChange', () => {
            drawChart();
        });
        projectEventEmitter.on('picado:toggleLoader', (options) => {
            $elements.loader.style.display = options.mode;
            if (options.theme && options.theme == 'dark') {
                $elements.loader.style.backgroundColor = '#212121';
            } else {
                $elements.loader.style.backgroundColor = '#ffffff';
            }
            if (!options.splash) {
                $elements.loader.classList.remove('splash');
                $elements.loader.getElementsByClassName('far')[0].style.display = 'none';
            }
        });
    }

    Bootstrap.prototype.bootstrapWorker = async function() {
        projectEventEmitter.emit('picado:toggleLoader', { mode: 'flex', splash: true });

        $elements.createNewProjectBtn.addEventListener("click", function(e) {
            e.preventDefault();
            ipcRenderer.send('project:new-project', true);
        });
        $elements.createProjectFromURL.addEventListener("click", function(e) {
            e.preventDefault();
            let modal = new PicadoModal('', 'Ouvrir depuis un URL', 'Ouvrez une image à partir d\'un URL :', [{
                name: 'Ourvir',
                secure: false,
                do: () => {
                    let URL = modal.sharedElement.input[0].elementRef.value;
                    let project = {
                        name: "",
                        url: URL,
                        lastModification: new Date()
                    };
                    dbInstance.create(project).then(() => {
                        picado.project = project;
                        picado.active = true;
                    }).then(() => {
                        projectEventEmitter.emit('picado:projectCreated', project);
                        render();
                        modal.close();
                    }).catch(err => {
                        console.error('Error', err);
                    });
                }
            }], [{
                type: 'text',
                placeholder: 'Entrez l\'URL...'
            }]);
            modal.show();
        });
        $elements.gettingStartedSection.style.visibility = 'visible';

        render();

        elementsGetter.byId(`dispMode__${picado.displayProject}`).classList.add("active");
    }

    return Bootstrap;
})();

(function() {
    ipcRenderer.on('project:project-created', function(event, path) {
        let { filePaths } = path;
        const sliptedUrl = filePaths[0].split('\\');
        const fileName = sliptedUrl[sliptedUrl.length - 1];
        const title = `Edition: ${fileName} - Picado`;
        $elements.appTitle.innerText = title;

        let project = {
            name: fileName,
            url: securizeUrl(filePaths[0]),
            lastModification: new Date()
        }

        dbInstance.create(project).then(() => {
            picado.project = project;
            picado.active = true;
        }).then(() => {
            projectEventEmitter.emit('picado:projectCreated', picado.project);
            render();
        }).catch(err => {
            console.error("Error", err);
        });

    });

    let bootstraping = new Bootstrap();

    bootstraping.init().then(() => {
        bootstraping.initEmitterEvent();
        bootstraping.bootstrapWorker().then(() => {
            setTimeout(() => {
                projectEventEmitter.emit('picado:toggleLoader', { mode: 'none' });
            }, 2000);
        }).finally(() => {
            $elements.appTitle.textContent = picado.active === false ? `Démarer - Picado v${ picado.version }` : `Mode édition - Picado v${ picado.version }`;
        });
    });
}).call(this);

function securizeUrl(url) {
    return url.split('\\').join('/');
}

function getElapsedTime(date = new Date()) {
    const seconds = Math.floor((+new Date() - +date) / 1000);
    if (seconds < 29) return 'à l\'instant';
    const intervals = {
        'ans': 31536000,
        'mois': 2592000,
        'semaine': 604800,
        'jour': 86400,
        'heure': 3600,
        'minute': 60,
        'seconde': 1
    };
    let counter;
    for (const i in intervals) {
        counter = Math.floor(seconds / intervals[i]);
        if (counter > 0) {
            if (counter === 1) {
                return `il y a ${counter} ${i}`;
            } else {
                return `il y a ${counter} ${i}s`;
            }
        }
    }
}

function runTimer(date = new Date(), target) {
    target.innerText = getElapsedTime(date);
    return setTimeout(() => {
        runTimer(date, target);
    }, 30000);
}

function toggleProjectDisplayMode(event, displayAs) {
    let elementsGetter = new ViewElementsSelector();
    if (displayAs !== picado.displayProject) {
        event.preventDefault();

        elementsGetter.byId(`recentlyProjectNode__${picado.displayProject}`).style.display = 'none';
        elementsGetter.byId(`dispMode__${picado.displayProject}`).classList.remove("active");

        picado.displayProject = displayAs;
        render().then(() => {
            event.currentTarget.classList.add("active");
            localStorage.setItem("PROJECT_DISPLAY_MODE", displayAs);
        });
    }
}

async function render() {
    let elementGetter = new ViewElementsSelector();
    var projectNode = elementGetter.byId(`recentlyProjectNode__${picado.displayProject}`);

    dbInstance.readAll().then(projects => {
        projectNode.innerHTML = '';
        projectNode.style.display = 'flex'

        projects.map((project, pos) => {
            const div = document.createElement('div');
            const button = document.createElement('button');
            button.classList.add('close-btn');
            button.setAttribute("data-project", pos);
            const i = document.createElement('i');
            i.classList.add("far", "fa-trash");
            button.appendChild(i);

            if (picado.displayProject === 'list') {
                div.classList.add("project");
                const leftBlock = document.createElement('div');
                const rightBlock = document.createElement('div');
                leftBlock.classList.add("left");
                const name = document.createElement('span');
                name.classList.add("name");
                name.innerText = project.name;
                const info = document.createElement('span');
                info.innerText = `Ouvert ${getElapsedTime(project.lastModification)}`;
                info.classList.add("info");
                leftBlock.appendChild(name);
                leftBlock.appendChild(document.createElement('br'));
                leftBlock.appendChild(info);

                rightBlock.classList.add("right");
                rightBlock.appendChild(button);

                div.appendChild(leftBlock);
                div.appendChild(rightBlock);

                div.addEventListener('click', function(e) {
                    e.preventDefault();
                    picado.project = project;
                    picado.active = true;
                    projectEventEmitter.emit('picado:projectCreated', project);
                });

            } else {
                const img = document.createElement('img');
                img.src = project.url;
                img.onload = function() {
                    img.src = resize(img, 200);
                }
                img.addEventListener("click", function(e) {
                    e.preventDefault();
                    picado.project = project;
                    picado.active = true;
                    projectEventEmitter.emit('picado:projectCreated', project);
                });

                div.appendChild(img);
                div.appendChild(button);

            }
            button.addEventListener('click', function(e) {
                e.preventDefault();
                let modal = new PicadoModal('warning', "Supprimer un projet", "Est-ce-que vous vouliez vraiement supprimer ce projet ?", [{
                    name: "Oui, supprimez!",
                    secure: true,
                    do: () => {
                        dbInstance.delete(project._id).then(() => {
                            render();
                            modal.close();
                        }).catch(err => {
                            console.error("Error", err);
                        });
                    }
                }]);
                modal.show();
            });
            projectNode.appendChild(div);
        });
    });
}

function reload() {
    if (picado.project.traitementsQue.length > 0) {
        for (let i = 0; i < picado.project.traitementsQue.length; i++) {
            switch (picado.project.traitementsQue[i].name) {
                case 'initialisation':
                    {};
                    break;
                case 'inversion':
                    imageData.data = $.invert(imageData.data);
                    break;
                case 'greyscale':
                    imageData.data = $.greyScale(imageData.data);
                    break;
                case 'rotation':
                    imageData.data = $.rotate(imageData.data, newWidth, newHeight);
                    break;
                case 'horizontalSymetic':
                    imageData.data = $.horizontalSymetric(imageData.data, newWidth, newHeight);
                    break;
                case 'redFilter':
                    imageData.data = $.filterRed(imageData.data, newWidth, newHeight);
                    break;
                case 'greenFilter':
                    imageData.data = $.filterGreen(imageData.data, newWidth, newHeight);
                    break;
                case 'blueFilter':
                    imageData.data = $.filterBlue(imageData.data, newWidth, newHeight);
                    break;
                default:
                    '';
                    break;
            }
        }
        mainCtx.putImageData(imageData, 0, 0);
    }
}

function openCreateProject(project) {
    projectEventEmitter.emit('picado:toggleLoader', { mode: 'flex', theme: 'dark' });
    let begin = new Date();
    var elapsedTime = 0;
    openProjectWorker(project).then(() => {
        let end = new Date();
        elapsedTime = end - begin;
    }).finally(() => {
        projectEventEmitter.emit('picado:projectLoaded', { executionTime: elapsedTime, projectName: project.name });
        pannelWelcomingAnimate();
    });
}

async function getAndApplyTransf(type, data, args = {}) {
    return new Promise((success, error) => {
        switch (type) {
            case 'initialisation':
                success(data);
                break;
            case 'inversion':
                success($.invert(data));
                break;
            case 'greyscale':
                success($.greyScale(data));
                break;
            case 'rotation':
                $.rotate(data, args.width, args.height).then((result) => {
                    success(result);
                });
                break;
            case 'horizontalSymetric':
                success($.horizontalSymetric(data, args.width, args.height));
                break;
            case 'verticalSymetric':
                $.verticalSymetric(data, args.width).then((result) => {
                    success(result);
                });
                break;
            case 'redFilter':
                success($.filterRed(data));
                break;
            case 'greenFilter':
                success($.filterGreen(data));
                break;
            case 'blueFilter':
                success($.filterBlue(data));
                break;
            case 'linearFilter__lissage':
                $.linearFilter(data, args.width, 'lissage').then((result) => {
                    success(result);
                }, (error) => { error(error); });
                break;
            case 'linearFilter__convolution':
                $.linearFilter(data, args.width, 'convolution').then((result) => {
                    success(result);
                }, (error) => { error(error); });
                break;
            case 'linearFilter__accentuation':
                $.linearFilter(data, args.width, 'accentuation').then((result) => {
                    success(result);
                }, (error) => { error(error); });
                break;
            case 'linearFilter__gaussien3x3':
                $.linearFilter(data, args.width, 'gaussien3x3').then((result) => {
                    success(result);
                }, (error) => { error(error); });
                break;
            case 'linearFilter__rehausseur':
                $.linearFilter(data, args.width, 'rehausseur').then((result) => {
                    success(result);
                }, (error) => { error(error); });
                break;
            case 'linearFilter__laplacien':
                $.linearFilter(data, args.width, 'laplacien').then((result) => {
                    success(result);
                }, (error) => { error(error); });
                break;
            case 'linearFilter__sobel':
                $.linearFilter(data, args.width, 'sobel1x').then((result) => {
                    $.linearFilter(result, args.width, 'sobel2x').then((result) => {
                        success(result);
                    }, (error) => { throw error; });
                }, (error) => { throw error; });
                break;
            case 'median':
                $.filterMedian(data, args.width, 5).then((result) => {
                    success(result);
                });
                break;
            case 'etalementDynamique':
                $.etalementDynamique(data).then((result) => {
                    success(result);
                });
                break;
            case 'contouringFilter':
                success(data);
                // $.contouring(data, args.width).then((result) => {
                //     success(result);
                // });
                break;
            case 'binarisationFilter':
                $.binarisation(data).then((result) => {
                    success(result);
                }, (error) => { error(error); });
                break;
            case 'flagEffect':
                $.flagging(data, args.width, args.height).then((result) => {
                    success(result);
                }, (error) => { error(error); });
                break;
            case 'addBlue':
                $.addBlue(data, args.width, args.height).then((result) => {
                    success(result);
                }, (error) => { throw error; });
                break;
            case 'erosion':
                $.erosion(data, args.width).then((result) => {
                    success(result);
                });
                break;
            case 'dilatation':
                $.dilatation(data, args.width).then((result) => {
                    success(result);
                });
                break;
            case 'contrast':
                $.contrast(data, args.ratio).then((result) => {
                    success(result);
                });
                break;
            case 'saturation':
                $.saturation(data, args.ratio).then((result) => {
                    success(result);
                });
                break;
            case 'luminosite':
                $.limunosite(data, args.ratio).then((result) => {
                    success(result);
                });
                break;
                // case 'contouringFilter':
                //     $.contouring(data, args.width).then((result) => {
                //         success(result);
                //     });
                //     break;
            default:
                success(imageData);
                break;
        }
    });
}

async function openProjectWorker(project) {
    const projectImageWidth = document.getElementById('project-image-width'),
        projectImageHeight = document.getElementById('project-image-height'),
        projectImageName = document.getElementById('project-image-name'),
        projectImageUrl = document.getElementById('project-image-url');

    const img = document.createElement('img');
    img.src = project.url;
    img.onload = function() {
        try {
            var wrh = img.width / img.height;
            var resizedWidth = $elements.mainCanvas.width;
            var resizedHeight = resizedWidth / wrh;
            if (resizedHeight > $elements.mainCanvas.height) {
                resizedHeight = $elements.mainCanvas.height;
                resizedWidth = resizedHeight * wrh;
            }

            picado.width = resizedWidth * .75;
            picado.height = resizedHeight * .75;

            $elements.mainCanvas.width = picado.width;
            $elements.mainCanvas.height = picado.height;

            mainCtx.drawImage(img, 0, 0, picado.width, picado.height);

            imageData = mainCtx.getImageData(0, 0, picado.width, picado.height);

            if (project.transformations && project.transformations.length > 0) {
                project.transformations.map((transformation, pos) => {
                    let type = transformation.name;
                    let args = {};

                    if (picado.transfNeedExtradata.test(type) === true) {
                        args.width = picado.width;
                        args.height = picado.height;

                        if (picado.transfNeedRatio.test(type) === true) {
                            args.ratio = transformation.extraData.ratio;
                        }
                    }

                    getAndApplyTransf(type, imageData.data, args).then((result) => {
                        for (let i = 0; i < imageData.data; i++) {
                            imageData.data[i] = result[i];
                        }
                    }).finally(() => {
                        if (pos == project.transformations.length - 1) {
                            mainCtx.putImageData(imageData, 0, 0);
                            originalData = mainCtx.getImageData(0, 0, picado.width, picado.height);
                            console.log(`Project ${project.name} loaded.`);
                        }
                    });
                });
            } else {
                originalData = mainCtx.getImageData(0, 0, picado.width, picado.height);
                console.log(`[LOG][Picado-app] : Project ${project.name} loaded.`);
            }

            picado.cache[0] = imageData;

            toggleComparaisonMode(picado.comparaisonProjectMode);

            for (let i = 0; i < $elements.ranges.length; i++) {
                $elements.ranges[i].value = 0;
            }

            projectImageName.innerText = project.name;
            projectImageUrl.innerHTML = project.url;
            projectImageWidth.innerText = img.naturalWidth;
            projectImageHeight.innerText = img.naturalHeight;

            picado.project = {...picado.project, natWidth: img.naturalWidth, natHeight: img.naturalHeight, traitementsQue: project.transformations && project.transformations.length > 0 ? project.transformations : [] };

            bindHistory(picado.project.traitementsQue);

            $elements.mainCanvas.onmousemove = trackMousePosInImage;

            picado.updatedAt = project.lastModification;
            timer = runTimer(project.lastModification, $elements.elapsedTimeLabel);
            updateSaveState();

            initThumbnails(img);

            picado.img = img;

            drawChart();
        } catch (e) {
            console.error(e);
            let modal = new PicadoModal("danger", "Erreur interne", `Désolé, une erreur interne s'est produite lors du lancement de ce projet.`, [{
                name: 'Relancer Picado',
                secure: true,
                do: () => {
                    ipcRenderer.send('picado:force-reload', null);
                    modal.close();
                }
            }]);
            modal.show();
        }
    }
}

function drawChart() {
    $.getHisto(imageData.data).then((layersHistograms) => {
        let histogramsOfLayers = layersHistograms;
        let ctx = document.getElementById('histogram-chart').getContext('2d');

        let chartLabel = [];
        for (let i = 0; i <= 255; i++) {
            chartLabel.push(i);
        }

        var chart = new Chart(ctx, {
            type: 'line',
            data: {
                labels: chartLabel,
                datasets: [{
                    label: "Rouge",
                    data: histogramsOfLayers.r,
                    fill: false,
                    borderColor: 'rgba(255, 99, 132, 255)'
                }, {
                    label: "Vert",
                    data: histogramsOfLayers.v,
                    fill: false,
                    borderColor: 'rgba(75, 192, 192, 255)'
                }, {
                    label: "Bleu",
                    data: histogramsOfLayers.b,
                    fill: false,
                    borderColor: 'rgba(54, 162, 235, 255)'
                }]
            },
            options: {
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true
                        }
                    }]
                }
            }
        });
    }, (error) => {
        throw error;
    });
}

async function toggleComparaisonMode(mode, extra = {}) {
    unableAllComparaisonMode();
    switch (mode) {
        case 'single':
            {}
            break;
        case 'sideBySide':
            {
                $elements.mainCanvasSecondary.width = picado.width;
                $elements.mainCanvasSecondary.height = picado.height;

                $elements.mainCanvasSecondary.src = extra.url ? extra.url : picado.img.src;
                $elements.mainCanvasSecondary.onload = () => {
                    $elements.mainCanvasSecondary.style.display = 'flex';
                }
            }
            break;
        case 'slider':
            {}
            break;
        default:
            {}
            break;
    }
}

function unableAllComparaisonMode() {
    $elements.mainCanvasSecondary.style.display = 'none';
}

function changeComparaisonMode(event, mode) {
    let els = document.querySelectorAll('.viewer-mode-container .viewer-mode');
    for (let i = 0; i < els.length; i++) {
        els[i].classList.toggle('active', false);
    }
    let el = event.currentTarget;
    el.classList.toggle('active', true);
    toggleComparaisonMode(mode);
}


function bindHistory(transformations = []) {
    $elements.traitementHistoricsNode.innerHTML = '';
    transformations.map((history, pos) => {
        var historyEl = document.createElement('div');
        historyEl.classList.add('history');
        var contentEl = document.createElement('div');
        contentEl.classList.add('content');
        var titleEl = document.createElement('span');
        titleEl.classList.add('title');
        titleEl.innerText = getTransRealName(history.name);
        var descEl = document.createElement('span')
        descEl.classList.add('desc');
        descEl.innerText = `Ajouté ${getElapsedTime(history.addedAt)}`;
        contentEl.appendChild(titleEl);
        contentEl.appendChild(document.createElement('br'));
        contentEl.appendChild(descEl);
        historyEl.appendChild(contentEl);
        $elements.traitementHistoricsNode.appendChild(historyEl);
    });
}

function getTransRealName(trans) {
    switch (trans) {
        case 'initialisation':
            {};
            break;
        case 'inversion':
            return 'Inversion des couleurs';
            break;
        case 'greyscale':
            return 'Inversion des niveaux de girs';
            break;
        case 'rotation':
            return 'Rotation';
            break;
        case 'horizontalSymetic':
            ;
            return 'Symetrie Horizontale';
            break;
        case 'redFilter':
            return 'Filtrer la couleur Rouge';
            break;
        case 'greenFilter':
            return 'Filtrer la couleur Verte';
            break;
        case 'blueFilter':
            return 'Filtrer la couleur Bleu';
            break;
        case 'linearFilter__lissage':
            return 'Filtrer lissage';
            break;
        case 'linearFilter__convolution':
            return 'Filtrer convolution';
            break;
        case 'linearFilter__accentuation':
            return 'Filtrer accentuation';
            break;
        case 'linearFilter__gaussien3x3':
            return 'Filtrer gaussien';
            break;
        case 'linearFilter__rehausseur':
            return 'Filtrer rehausseur';
            break;
        case 'linearFilter__laplacien':
            return 'Filtrer laplacien';
            break;
        case 'binarisationFilter':
            return 'Filtrer binarisation';
            break;
        case 'flagEffect':
            return 'Effet drapeau';
            break;
        case 'addBlue':
            return 'Effet aquamarine';
            break;
        case 'erosion':
            return 'Effet erosion';
            break;
        case 'dilatation':
            return 'Effet dilatation';
            break;
        case 'contouringFilter':
            return 'Contourage';
            break;
        default:
            '';
            break;
    }
}

function closeRuningProject() {
    let modal = new PicadoModal('info', 'Fermer le projet en cours', 'Voulez-vous fermer ce projet et enregister les modifications ?', [{
        name: 'Fermez',
        secure: true,
        do: () => {
            console.clear();
            unsetAllTools();
            $elements.editorSection.style.visibility = 'hidden';
            document.body.classList.remove('__dark');
            $elements.gettingStartedSection.style.display = 'flex';
            $elements.mainCanvas.width = 800;
            $elements.mainCanvas.height = 800;
            clearTimeout(timer);
            picado.haveModification = false;
            picado.active = false;
            $elements.viewerModeContainer.classList.toggle('active', false);
            $elements.appTitle.textContent = `Démarer - Picado v${ picado.version }`;
            modal.close();
        }
    }]);
    modal.show();
}

function openUrlEvenIfRuning() {
    let modal = new PicadoModal('info', 'Ouvrir un URL', 'Voulez-vous fermer ce projet et ourvrir un URL ?', [{
        name: 'Ouvrir',
        secure: true,
        do: () => {
            modal.close();
            $elements.createProjectFromURL.click();
        }
    }]);
    modal.show();
}

function handleZoomChange(event) {
    if (picado.temporaryCanvas == null) {
        picado.temporaryCanvas = $elements.mainCanvas;
    }
    const value = parseInt(event.currentTarget.value);
    var ratio = value / 100;
    if (ratio >= 0) {
        ratio += 1;
    } else {
        ratio *= -1;
        ratio = 1 - ratio;
    }
    let img = new Image();
    img.src = picado.temporaryCanvas.toDataURL("image/jpg");
    img.onload = () => {
        let tempWidth = picado.width * ratio;
        let tempHeight = picado.height * ratio;

        $elements.mainCanvas.width = tempWidth;
        $elements.mainCanvas.height = tempHeight;

        mainCtx.drawImage(img, 0, 0, $elements.mainCanvas.width, $elements.mainCanvas.height);
    }
}

function zoomOut() {
    if (picado.temporaryCanvas == null) {
        picado.temporaryCanvas = $elements.mainCanvas;
    }
    let img = new Image();
    img.src = picado.temporaryCanvas.toDataURL("image/jpg");
    img.onload = () => {
        let ratio = 0.75
        picado.width = picado.width * ratio;
        picado.height = picado.height * ratio;

        $elements.mainCanvas.width = picado.width;
        $elements.mainCanvas.height = picado.height;

        mainCtx.drawImage(img, 0, 0, $elements.mainCanvas.width, $elements.mainCanvas.height);
    }
}

function zoomIn() {
    if (picado.temporaryCanvas == null) {
        picado.temporaryCanvas = $elements.mainCanvas;
    }
    let img = new Image();
    img.src = picado.temporaryCanvas.toDataURL("image/jpg");
    img.onload = () => {
        let ratio = 1.25;
        picado.width = picado.width * ratio;
        picado.height = picado.height * ratio;

        $elements.mainCanvas.width = picado.width;
        $elements.mainCanvas.height = picado.height;

        mainCtx.drawImage(img, 0, 0, $elements.mainCanvas.width, $elements.mainCanvas.height);
    }
}

function pannelWelcomingAnimate() {
    let selector = new ViewElementsSelector();
    let pannels = ['histogram', 'amelioration'];
    if (picado.project.transformations && picado.project.transformations.length > 0) {
        pannels = [...pannels, 'historics'];
    }
    pannels.map((id, index) => {
        id = `pannel_${ id }`;
        let el = selector.byId(id);
        if (el) {
            if (el.classList.contains('hide') == false) {
                el.classList.add('hide');
            }
            setTimeout(() => {
                el.classList.toggle('hide', false);
                clearTimeout();
            }, index * 200);
        }
    });
}

async function draw(ratio) {
    mainCtx.clearRect(0, 0, picado.width, picado.height);
    mainCtx.scale(ratio, ratio);
    mainCtx.drawImage(picado.img, 0, 0, picado.width, picado.height);
}

function openProjectEvenIfRuning() {
    if (picado.haveModification) {
        let modal = new PicadoModal('info', 'Ouvrir une image', 'Voulez-vous fermer ce projet et ouvrir un autre ?', [{
            name: 'Fermez et Ouvrir',
            secure: true,
            do: () => {
                saveCurrentProject();
                ipcRenderer.send('project:new-project', true);
                initializeCanvas();
                modal.close();
            }
        }]);
        modal.show();
    } else {
        ipcRenderer.send('project:new-project', true);
        initializeCanvas();
    }
}

function initializeCanvas() {
    $elements.mainCanvas.width = 800;
    $elements.mainCanvas.height = 800;
}

function saveCurrentProject() {
    if (picado.project.traitementsQue.length > 0) {
        let data = {
            _id: picado.project._id,
            lastModification: new Date(),
            transformations: picado.project.traitementsQue
        }
        dbInstance.updateTraitements(data).then(() => {
            originalData = mainCtx.getImageData(0, 0, picado.width, picado.height);
            picado.updatedAt = data.lastModification;
            picado.haveModification = false;
            bindHistory(picado.project.traitementsQue);
        }).then(() => {
            updateSaveState();
            clearTimeout(timer);
            timer = runTimer(picado.updatedAt, $elements.elapsedTimeLabel);
        }).catch((err) => {
            console.error("app.saveCurrentProject", err);
        });
    }
}

function updateSaveState() {
    let $this = $elements.saveStateLabel;
    if (picado.haveModification === false) {
        $this.classList.remove('warning');
        $this.classList.add('success');
        $this.innerText = 'Enregistré';
    } else {
        $this.classList.remove('success');
        $this.classList.add('warning');
        $this.innerText = 'Non enregistré';
    }
}

function trackMousePosInImage(e) {
    var posX = 0;
    var posY = 0;
    var canvasPos = findPosition($elements.mainCanvas);
    if (!e) var e = window.event;
    if (e.pageX || e.pageY) {
        posX = e.pageX;
        posY = e.pageY;
    } else if (e.clientX || e.clientY) {
        posX = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
        posY = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
    }
    posX = posX - canvasPos[0];
    posY = posY - canvasPos[1];

    $elements.cursorPosXTracker.innerText = posX;
    $elements.cursorPosYTracker.innerText = posY;
}

function findPosition(el) {
    if (typeof(el.offsetParent) != "undefined") {
        for (var posX = 0, posY = 0; el; el = el.offsetParent) {
            posX += el.offsetLeft;
            posY += el.offsetTop;
        }
        return [posX, posY];
    } else {
        render[el.x, el.y];
    }
}

function exportImage() {
    let splitedName = picado.project.name.split('.');
    let ext = isValidExtension(splitedName[splitedName.length - 1]) ? splitedName[splitedName.length - 1] : null;

    if (ext) {
        let filters = { name: `Ficihier image JPEG`, extensions: ['jpg'] };
        ipcRenderer.send('project:get-export-path', { name: picado.project.name, filters: filters });

        ipcRenderer.on('project:give-export-path', function(event, path) {
            let begin = new Date();
            $elements.traitementBar.innerText = 'Exportation...';
            $elements.traitementBar.parentElement.style.display = 'flex';

            var exportImgEl = new Image();
            exportImgEl.src = picado.project.url;
            exportImgEl.onload = () => {
                try {
                    let exportCanvas = document.createElement('canvas');
                    let width = exportImgEl.naturalWidth;
                    let height = exportImgEl.naturalHeight;

                    exportCanvas.width = width;
                    exportCanvas.height = height;

                    let exportCanvasCtx = exportCanvas.getContext('2d');

                    exportCanvasCtx.drawImage(exportImgEl, 0, 0, width, height);

                    var data = exportCanvasCtx.getImageData(0, 0, width, height).data;
                    var emptyImageData = createEmptyImageData(data.length, width, height);

                    var imageTraitements;
                    if (picado.project.traitementsQue && picado.project.traitementsQue.length > 0) {
                        imageTraitements = picado.project.traitementsQue;
                    }

                    if (imageTraitements) {
                        imageTraitements.map((traitement, pos) => {
                            let type = traitement.name;
                            let args = {};

                            if (picado.transfNeedExtradata.test(type) === true) {
                                args.width = width;
                                args.height = height;
                            } else if (picado.transfNeedRatio.test(type) === true) {
                                args.ratio = traitement.extraData.ratio;
                            }
                            getAndApplyTransf(type, data, args).then((result) => {
                                for (let i = 0; i < data.length; i++) {
                                    emptyImageData.data[i] = result[i];
                                    data[i] = result[i];
                                }
                            }).finally(() => {
                                exportCanvasCtx.putImageData(emptyImageData, 0, 0);
                                if (pos == imageTraitements.length - 1) {
                                    let url = exportCanvas.toDataURL(`image/jpg`);
                                    ipcRenderer.send('project:export', { url: url, path: path });
                                }
                            });
                        });
                    }

                    ipcRenderer.on('project:success-export', function(event, path) {
                        let end = new Date();
                        document.getElementById('picado-alert-msg').innerText = `Emplacement : ${path}`;
                        setTimeout(() => {
                            $elements.traitementBar.parentElement.style.color = '#69F0AE';
                            let traitFaIcon = document.getElementById('traitementBarFaIcon');
                            traitFaIcon.classList.remove('fa-spin');
                            traitFaIcon.classList.replace('fa-circle-notch', 'fa-check');
                            $elements.traitementBar.innerText = `Exportation terminé! Durée: ${((end-begin) / (60 * 10)).toFixed(1)}s`;
                            $elements.picadAlert.classList.add('success');
                            $elements.picadAlert.classList.remove('hide');
                            $elements.picadAlert.classList.add('show');
                            setTimeout(() => {
                                $elements.traitementBar.parentElement.style.display = 'none';
                                $elements.traitementBar.parentElement.style.color = '#ffffff';
                                let traitFaIcon = document.getElementById('traitementBarFaIcon');
                                traitFaIcon.classList.add('fa-spin');
                                traitFaIcon.classList.replace('fa-check', 'fa-circle-notch');
                                $elements.picadAlert.classList.remove('show');
                                $elements.picadAlert.classList.add('hide');
                                clearTimeout();
                            }, 8000);
                            clearTimeout();
                        }, 1000);
                    });

                } catch (e) {
                    console.error(e);
                    let modal = new PicadoModal("danger", "Erreur interne", `Désolé, une erreur interne s'est produite lors de l'exportation de ce projet.`, [{
                        name: 'Relancer Picado',
                        secure: true,
                        do: () => {
                            modal.close();
                        }
                    }]);
                    modal.show();
                }
            }
        });
    }
}

function isValidExtension(ext) {
    return ext === 'jpg' || ext === 'JPG' || ext === 'png' || ext === 'PNG' ? true : false;
}

function undo() {
    mainCtx.putImageData(picado.cache[0], 0, 0);
}

function redo() {
    if (picado.cache[1] != undefined) {
        mainCtx.putImageData(picado.cache[1], 0, 0);
    }
}

function toggleTools(event, tools) {
    picado.startingQueIndex = picado.project.traitementsQue ? picado.project.traitementsQue.length - 1 : 0;
    var i, panTools, panLinks;

    panTools = document.getElementsByClassName('tools-option');
    for (i = 0; i < panTools.length; i++) {
        panTools[i].classList.remove("show");
        panTools[i].classList.remove("hide");
    }

    panLinks = document.getElementsByClassName('link-tool');
    for (i = 0; i < panLinks.length; i++) {
        panLinks[i].className = panLinks[i].className.replace('active', "");
    }

    document.getElementById(tools).classList.add('show');
    event.currentTarget.classList.add('active');
}

function removeTools(tools) {
    if (picado.haveModification === true) {
        const modal = new PicadoModal('info', 'Appliquer les modifications', 'Voulez-vous appliquers les modifications sur cette image ?', [{
            name: 'Appliquer',
            secure: true,
            do: () => {
                saveTools(tools);
                modal.close();
            }
        }, {
            name: 'Ne pas appliquer',
            secure: false,
            do: () => {
                document.getElementById(tools).classList.add('hide');
                document.getElementById(tools).classList.remove('show');

                var panLinks = document.getElementsByClassName('link-tool');
                for (i = 0; i < panLinks.length; i++) {
                    panLinks[i].className = panLinks[i].className.replace('active', "");
                }
                var itemTools = document.getElementsByClassName('item-tool');
                for (let i = 0; i < itemTools.length; i++) {
                    itemTools[i].classList.remove('active');
                }
                picado.haveModification = false;

                picado.project.traitementsQue.splice(picado.startingQueIndex, picado.project.traitementsQue.slice(picado.startingQueIndex, picado.project.traitementsQue.length - 1).length);

                addTransInitialize();

                modal.close();
            }
        }]);
        modal.show();
    } else {
        document.getElementById(tools).classList.add('hide');
        document.getElementById(tools).classList.remove('show');

        var panLinks = document.getElementsByClassName('link-tool');
        for (i = 0; i < panLinks.length; i++) {
            panLinks[i].className = panLinks[i].className.replace('active', "");
        }
    }
}

function saveTools(tools) {
    if (picado.haveModification === true) {

        originalData = mainCtx.getImageData(0, 0, $elements.mainCanvas.width, $elements.mainCanvas.height);
        imageData = mainCtx.getImageData(0, 0, $elements.mainCanvas.width, $elements.mainCanvas.height);

        document.getElementById(tools).classList.add('hide');
        document.getElementById(tools).classList.remove('show');

        var panLinks = document.getElementsByClassName('link-tool');
        for (i = 0; i < panLinks.length; i++) {
            panLinks[i].className = panLinks[i].className.replace('active', "");
        }
        var itemTools = document.getElementsByClassName('item-tool');
        for (let i = 0; i < itemTools.length; i++) {
            itemTools[i].classList.remove('active');
        }

        originalData = mainCtx.getImageData(0, 0, picado.width, picado.height);

        updateSaveState();
        bindHistory(picado.project.traitementsQue);
        picado.haveModification = false;

        projectEventEmitter.emit('picado:projectHaveChange', null);
    } else {
        document.getElementById(tools).classList.add('hide');
        document.getElementById(tools).classList.remove('show');

        var panLinks = document.getElementsByClassName('link-tool');
        for (i = 0; i < panLinks.length; i++) {
            panLinks[i].className = panLinks[i].className.replace('active', "");
        }
    }
}

function unsetAllTools() {
    var panTools = document.getElementsByClassName('tools-option');
    for (i = 0; i < panTools.length; i++) {
        panTools[i].classList.remove("show");
        panTools[i].classList.remove("hide");
    }

    var panLinks = document.getElementsByClassName('link-tool');
    for (i = 0; i < panLinks.length; i++) {
        panLinks[i].className = panLinks[i].className.replace('active', "");
    }
    var tools = document.getElementsByClassName('item-tool');
    for (i = 0; i < tools.length; i++) {
        tools[i].className = tools[i].className.replace('active', "");
    }
}

function toogleToolPanel(event, pannelId) {
    let selector = new ViewElementsSelector();
    let el = selector.byId(`pannel_${ pannelId }`);
    if (el.classList.contains('hide') == false) {
        el.classList.toggle('hide', true);
        event.target.classList.remove('fa-chevron-up');
        event.target.classList.add('fa-chevron-down');
    } else {
        el.classList.toggle('hide', false);
        event.target.classList.remove('fa-chevron-down');
        event.target.classList.add('fa-chevron-up');
    }
}

function initThumbnails(image = new Image()) {
    var imgs = document.getElementsByClassName('cover-img');
    for (let i = 0; i < imgs.length; i++) {
        let typeOfTransf = imgs[i].getAttribute('data-transformation');
        var targetImg = new Image();
        targetImg.src = resize(image, 100);
        targetImg.onload = function() {
            var tmpCanvas = document.createElement('canvas');
            tmpCanvas.width = targetImg.width;
            tmpCanvas.height = targetImg.height;
            var tmpCanCtx = tmpCanvas.getContext('2d');
            tmpCanCtx.drawImage(targetImg, 0, 0, tmpCanvas.width, tmpCanvas.height);
            var targetImageData = tmpCanCtx.getImageData(0, 0, tmpCanvas.width, tmpCanvas.height);

            let args = {};
            if (/rotation|horizontalSymetric|verticalSymetric|linearFilter__lissage|linearFilter__convolution|linearFilter__gaussien3x3|linearFilter__rehausseur|linearFilter__laplacien|linearFilter__accentuation|linearFilter__sobel|addBlue|flagEffect|erosion|dilatation|contouringFilter|median/.test(typeOfTransf) === true) {
                args.width = targetImg.width;
                args.height = targetImg.height;
            }
            getAndApplyTransf(typeOfTransf, targetImageData.data, args).then((result) => {
                if (result) {
                    for (let i = 0; i < targetImageData.data; i++) {
                        targetImageData.data[i] = result[i];
                    }
                } else {
                    throw new Error(`[INTERNAL_ERROR] : Can't have result for ${ typeOfTransf }.`);
                }
            }, (error) => {
                throw new Error(error);
            }).then(() => {
                tmpCanCtx.putImageData(targetImageData, 0, 0);
            }).finally(() => {
                imgs[i].src = tmpCanvas.toDataURL('image/png');
            });
        }
    }
}

function resize(image = new Image(), size) {
    var canvas = document.createElement('canvas');
    canvas.width = image.width;
    canvas.height = image.height;
    var ctx = canvas.getContext('2d');
    ctx.drawImage(image, 0, 0, canvas.width, canvas.height);
    while (canvas.width > size) {
        canvas = halfSize(canvas);
    }
    return canvas.toDataURL("image/jpg");
}

function halfSize(i) {
    var canvas = document.createElement('canvas');
    canvas.width = i.width / 2;
    canvas.height = i.height / 2;
    var ctx = canvas.getContext('2d');
    ctx.drawImage(i, 0, 0, canvas.width, canvas.height);
    return canvas;
}

function createEmptyImageData(length, dw, dh) {
    let data = new ImageData(dw, dh);
    data.data = new Uint8ClampedArray(length);
    return data;
}

/* TRANSFORMATIONS */

function addTransInitialize(event = null, options = {}) {
    if (event) {
        event.preventDefault();
    }
    let operation = new Promise((success, error) => {
        let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
        for (let i = 0; i < originalData.data.length; i++) {
            resultImageData.data[i] = originalData.data[i];
        }
        success(resultImageData);
        error(new Error('[ERROR][addTransInitialize]: Can\'t initialize image data.'));
    });
    operation.then((result) => {
        mainCtx.putImageData(result, 0, 0);
    }).finally(() => {
        picado.haveModification = false;
        if (options.operation && options.operation == 'Amelioration') {

        } else {
            picado.project.traitementsQue = [];
            var items = document.getElementsByClassName('item-tool');
            for (let i = 0; i < items.length; i++) {
                items[i].classList.remove('active');
            }
        }
    });
    //projectEventEmitter.emit('picado:projectHaveChange', null);
}

function addLinearFilter(event, filtre) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.linearFilter(originalData.data, picado.width, filtre).then((result) => {
        for (let i = 0; i < imageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
    }, (error) => {
        throw error;
    }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;
        picado.project.traitementsQue.push({
            name: 'linearFilter__' + filtre,
            addedAt: new Date(),
            extraData: null
        });
    });
}

function addTransInvert(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    let result = $.invert(originalData.data);
    for (let i = 0; i < result.length; i++) {
        resultImageData.data[i] = result[i];
    }

    mainCtx.putImageData(resultImageData, 0, 0);
    picado.haveModification = true;
    picado.project.traitementsQue.push({
        name: 'inversion',
        addedAt: new Date(),
        extraData: null
    });
}

function addGreyScale(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.greyScale(originalData.data).then((result) => {
        for (let i = 0; i < result.length; i++) {
            resultImageData.data[i] = result[i];
        }
    }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;
        picado.project.traitementsQue.push({
            name: 'greyscale',
            addedAt: new Date(),
            extraData: null
        });
    });
}

function addTransRotate(event) {
    let angle = 0;
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.rotate(originalData.data, picado.width, picado.height, angle).then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
        // $elements.mainCanvas.width = picado.height;
        // $elements.mainCanvas.height = picado.width;

        // mainCtx = $elements.mainCanvas.getContext('2d');

        // imageData.data.width = picado.height;
        // imageData.data.height = picado.width;
    }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;

        picado.project.traitementsQue.push({
            name: 'rotation',
            addedAt: new Date(),
            extraData: null
        });
    });
}

function addTransSymH(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    let result = $.horizontalSymetric(originalData.data, picado.width, picado.height);
    for (let i = 0; i < resultImageData.data.length; i++) {
        resultImageData.data[i] = result[i];
    }
    mainCtx.putImageData(resultImageData, 0, 0);
    picado.haveModification = true;

    picado.project.traitementsQue.push({
        name: 'horizontalSymetric',
        addedAt: new Date(),
        extraData: null
    });
}

function addTransSymV(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.verticalSymetric(originalData.data, picado.width).then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
    }, (error) => { throw error; }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;

        picado.project.traitementsQue.push({
            name: 'verticalSymetric',
            addedAt: new Date(),
            extraData: null
        });
    });
}

function addTransFilterRed(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    let filtredRedArray = $.filterRed(originalData.data, picado.width, picado.height);
    for (let i = 0; i < resultImageData.data.length; i++) {
        resultImageData.data[i] = filtredRedArray[i];
    }
    mainCtx.putImageData(resultImageData, 0, 0);
    picado.haveModification = true;

    picado.project.traitementsQue.push({
        name: 'redFilter',
        addedAt: new Date(),
        extraData: null
    })
}

function addTransFilterGreen(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    let filtredGreen = $.filterGreen(originalData.data, picado.width, picado.height);
    for (let i = 0; i < resultImageData.data.length; i++) {
        resultImageData.data[i] = filtredGreen[i];
    }
    mainCtx.putImageData(resultImageData, 0, 0);
    picado.haveModification = true;

    picado.project.traitementsQue.push({
        name: 'greenFilter',
        addedAt: new Date(),
        extraData: null
    })
}

function addTransFilterBlue(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    let filtredBlueArray = $.filterBlue(originalData.data, picado.width, picado.height);
    for (let i = 0; i < resultImageData.data.length; i++) {
        resultImageData.data[i] = filtredBlueArray[i];
    }
    mainCtx.putImageData(resultImageData, 0, 0);
    picado.haveModification = true;

    picado.project.traitementsQue.push({
        name: 'blueFilter',
        addedAt: new Date(),
        extraData: null
    })
}

function addBlueValue(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.addBlue(originalData.data, picado.width, picado.height).then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
    }, (error) => { throw error; }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;
        picado.project.traitementsQue.push({
            name: 'addBlue',
            addedAt: new Date(),
            extraData: null
        });
    });
}

function flagEffect(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.flagging(originalData.data, picado.width, picado.height).then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
    }, (error) => { throw error; }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;
        picado.project.traitementsQue.push({
            name: 'flagEffect',
            addedAt: new Date(),
            extraData: null
        });
    });
}

function addBinaryFilter(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.binarisation(originalData.data).then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
    }, (error) => {
        throw error;
    }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;
        picado.project.traitementsQue.push({
            name: 'binarisationFilter',
            addedAt: new Date(),
            extraData: null
        });
    });
}

function addContouring(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.contouring(originalData.data, picado.width).then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
    }, (error) => {
        throw error;
    }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;
        picado.project.traitementsQue.push({
            name: 'contouringFilter',
            addedAt: new Date(),
            extraData: null
        });
    });
}

function toggleEmeliorationsApplyUndoPannel() {
    let el = $elements.emeliorationsApplyUndoContainer;
    if (picado.emeliorationsIsModified == true && !picado.emeliorationsPannelIsActive) {
        el.style.display = 'flex';
        picado.emeliorationsPannelIsActive = true;
    } else if (picado.emeliorationsIsModified == false) {
        el.style.display = 'none';
        picado.emeliorationsPannelIsActive = false;
    }
}

function applyAmelioration() {
    ameliorationsQue.map((item, pos) => {
        if (item) {
            picado.project.traitementsQue.push(item);
            ameliorationsQue[pos] = undefined;
        }
    });
    picado.emeliorationsIsModified = false;
    toggleEmeliorationsApplyUndoPannel();
}

function undoAmelioration() {
    ameliorationsQue = new Array(3);
    for (let i = 0; i < $elements.ranges.length; i++) {
        $elements.ranges[i].value = 0;
    }
    addTransInitialize(window.event, { operation: 'Amelioration' });
    picado.emeliorationsIsModified = false;
    toggleEmeliorationsApplyUndoPannel();
}

function addLuminosity(event) {
    let ratio = event.currentTarget.value;
    if (ratio) {
        let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
        $.limunosite(originalData.data, ratio).then((result) => {
            for (let i = 0; i < resultImageData.data.length; i++) {
                resultImageData.data[i] = result[i];
            }
            picado.emeliorationsIsModified = true;
        }).finally(() => {
            mainCtx.putImageData(resultImageData, 0, 0);
            picado.haveModification = true;
            toggleEmeliorationsApplyUndoPannel();

            ameliorationsQue[0] = {
                name: 'luminosite',
                addedAt: new Date(),
                extraData: {
                    ratio: ratio
                }
            };
        }, (error) => {
            throw error;
        });
    } else {
        throw new Error('[ERROR][addLuminosity]: Can\'t get value from range at addLuminosity(...).');
    }
}

function addContrast(event) {
    let ratio = event.currentTarget.value;
    if (ratio) {
        let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
        $.contrast(originalData.data, ratio).then((result) => {
            for (let i = 0; i < resultImageData.data.length; i++) {
                resultImageData.data[i] = result[i];
            }
            picado.emeliorationsIsModified = true;
        }).finally(() => {
            mainCtx.putImageData(resultImageData, 0, 0);
            picado.haveModification = true;
            toggleEmeliorationsApplyUndoPannel();

            ameliorationsQue[1] = {
                name: 'contrast',
                addedAt: new Date(),
                extraData: {
                    ratio: ratio
                }
            };
        }, (error) => {
            throw error;
        });
    } else {
        throw new Error('[ERROR][addContrast]: Can\'t get value from range at addContrast(...).');
    }
}

function addSaturation(event) {
    var ratio = event.currentTarget.value;
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.saturation(originalData.data, ratio).then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
        picado.emeliorationsIsModified = true;
    }, (error) => {
        throw error;
    }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;
        toggleEmeliorationsApplyUndoPannel();

        ameliorationsQue[2] = {
            name: 'saturation',
            addedAt: new Date(),
            extraData: {
                ratio: ratio
            }
        };

    });
}

function addErosionFilter(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.erosion(originalData.data, picado.width).then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
    }, (error) => {
        throw error;
    }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;
        picado.project.traitementsQue.push({
            name: 'erosion',
            addedAt: new Date(),
            extraData: null
        });
    });
}

function addDilatation(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.dilatation(originalData.data, picado.width).then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
    }, (error) => {
        throw error;
    }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;
        picado.project.traitementsQue.push({
            name: 'dilatation',
            addedAt: new Date(),
            extraData: null
        });
    });
}

function addSobelFilter(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.linearFilter(originalData.data, picado.width, 'sobel1x').then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
        $.linearFilter(resultImageData.data, picado.width, 'sobel2x').then((result) => {
            for (let i = 0; i < resultImageData.data.length; i++) {
                resultImageData.data[i] = result[i];
            }
        }, (error) => { throw error; }).finally(() => {
            mainCtx.putImageData(resultImageData, 0, 0);

            picado.haveModification = true;
        });
    }, (error) => {
        throw error;
    });
}

function addMedianFilter(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.filterMedian(originalData.data, picado.width, 5).then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
    }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;
        picado.project.traitementsQue.push({
            name: 'median',
            addedAt: new Date(),
            extraData: null
        });
    });
}

function addEtalementDynamique(event) {
    event.currentTarget.classList.add('active');
    let resultImageData = createEmptyImageData(originalData.data.length, picado.width, picado.height);
    $.etalementDynamique(originalData.data).then((result) => {
        for (let i = 0; i < resultImageData.data.length; i++) {
            resultImageData.data[i] = result[i];
        }
    }).finally(() => {
        mainCtx.putImageData(resultImageData, 0, 0);
        picado.haveModification = true;
        picado.project.traitementsQue.push({
            name: 'etalementDynamique',
            addedAt: new Date(),
            extraData: null
        });
    });
}