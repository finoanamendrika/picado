const electronInstaller = require('electron-winstaller');
const path = require('path');
try {
    electronInstaller.createWindowsInstaller({
        appDirectory: path.join(__dirname, './out/picado-win32-x64'),
        outputDirectory: './out/picado-win32-x64',
        authors: 'Picado Teams',
        exe: 'picado.exe'
    });
    console.log("It worked!")
} catch (e) {
    console.log("No dice:", e.message);
}