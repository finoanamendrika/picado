const projectItemSchema = {
    type: 'object',
    propreties: {
        name: {
            type: 'string'
        },
        url: {
            type: 'string'
        },
        lastModification: {
            type: 'date'
        },
        transformations: {
            type: 'array'
        }
    }
};

module.exports = projectItemSchema;