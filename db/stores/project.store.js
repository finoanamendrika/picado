const Datastore = require('nedb-promises');
const Ajv = require('ajv');
const projectItemSchema = require('../schemas/project.schema');

class ProjectItemStore {
    constructor() {
        const ajv = new Ajv({
            allErrors: true,
            useDefaults: true
        });

        this.schemaValidator = ajv.compile(projectItemSchema);
        const dbPath = `${process.cwd()}/db/databases/projects.db`;
        this.db = Datastore.create({
            filename: dbPath,
            timestampData: true
        });
    }

    validate(data) {
        return this.schemaValidator(data);
    }

    create(data) {
        const isValid = this.validate(data);
        if (isValid) {
            return this.db.insert(data);
        }
    }

    read(_id) {
        return this.db.findOne({ _id }).exec();
    }

    readAll() {
        return this.db.find().sort();
    }

    delete(_id) {
        return this.db.remove({ _id });
    }

    update({ _id, lastModification }) {
        return this.db.update({ _id }, { $set: { lastModification: lastModification } })
    }

    updateTraitements({ _id, lastModification, transformations }) {
        return this.db.update({ _id }, { $set: { lastModification: lastModification, transformations: transformations } });
    }
}

module.exports = new ProjectItemStore();